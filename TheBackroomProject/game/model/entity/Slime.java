package model.entity;

import info3.game.Game;
import model.Map;
import vue.AvatarSlime;

/**
 * Player : Représentation de l'entité slime
 */
public class Slime extends Enemy {
	/**
	 * Création d'un slime
	 * 
	 * @param l Ligne du slime
	 * @param c Colonne du slime
	 * @param m Map du slime
	 */
	public Slime(int l, int c, Map m) {
		super(l, c, m);
		this.maxHealth = 25;
		this.currentHealth = maxHealth;
		this.avatar = new AvatarSlime(this);

		switch (Game.difficulty) {
		case Game.EASY:
			this.damages = 25;
			break;
		case Game.HARD:
			this.damages = 40;
			break;
		case Game.NUKE:
			this.damages = 150;
			break;
		}
	}

	/**
	 * Déplace le slime aux coordonnées indiqués
	 * 
	 * @param l Ligne destination du slime
	 * @param c Colonne destination du slime
	 */
	@Override
	public void move(int l, int c) {
		this.line = l;
		this.column = c;
	}

	@Override
	public void pop(String str) {
		switch(str) {
		
		case "N":
		case "F":
			this.moveUp();
			return;
		case "E":
		case "R":
			this.moveRight();
			return;
		case "W":
		case "L":
			this.moveLeft();
			return;
		case "S":
		case "B":
			this.moveDown();
			return;
		default:
			return;
		}
	}

	@Override
	public void wizz(String str) {
		switch(str) {
			
			case "N":
			case "F":
				super.hitUp();
				return;
			
			case "E":
			case "R":
				super.hitRight();
				return;
				
			case "W":
			case "L":
				super.hitLeft();
				return;
				
			case "S":
			case "B":
				super.hitDown();
				return;
		}
	}

	@Override
	public int getTypeEntity() {
		return TypeEntity.SLIME;
	}

	@Override
	public int getRange() {
		return 15;
	}
	
	public boolean isHit() {
		this.currentHealth = 0;
		this.isDead();
		return true;
	}

	@Override
	public int getMaxHeath() {
		return this.maxHealth;
	}

	@Override
	public int getCurrentHealth() {
		return this.currentHealth;
	}

	@Override
	public void setCurrentHealth(int newHealth) {
		this.currentHealth = newHealth;
	}

	@Override
	public boolean isDead() {
		if(this.currentHealth <= 0) {
			this.deadEntity();
			return true;
		}
		return false;
	}

	@Override
	public int hit() {
		return this.damages;
	}

	@Override
	public int isHit(Entity e) {
		return this.currentHealth -= e.damages;
	}

	@Override
	public void deadEntity() {
		this.getAutomata().getController().getGame().addRemoveEntities(this);
	}

	@Override
	public boolean explode() {
		return false;
	}

}