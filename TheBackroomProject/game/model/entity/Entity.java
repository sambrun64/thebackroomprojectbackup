package model.entity;

import java.awt.Graphics;

import automataEngine.AutomatonExe;
import automataEngine.StateExe;

import model.GameItem;
import model.Map;
import model.NormalGround;
import model.TypeCase;

import vue.AvatarEntity;

/**
 * Entity : Représentation abstraite d'une entité. Une entité est un gameItem,
 * elle sera sur une case du jeu. Une entité est capable de se déplacer.
 */
public abstract class Entity extends GameItem {
	/* Comportement de l'entité */
	protected AutomatonExe automata;
	protected StateExe currentState;
	
	protected int maxHealth;
	protected int currentHealth;
	protected int damages;
	
	protected AvatarEntity avatar;
	
	public void askForPaint(Graphics g, int x, int y, int dx, int dy) {
		avatar.paint(g, x, y, dx, dy);
	}
	
	/**
	 * Création d'une entité
	 * 
	 * @param l Ligne de la case
	 * @param c Colonne de la case
	 * @param m Map sur laquel va évoluer l'entité
	 */
	public Entity(int l, int c, Map m) {
		super(l, c, m);
	}

	/**
	 * Deplace l'entité vers le Nord
	 */
	public boolean moveUp() {
		if (avatar.moving) {
			return false;
		}
		this.avatar.setOrientation("N");
		if(this.map.moveUp(this)) {
			this.avatar.setMoving(true);
			return true;
		}
		return false;
	}

	/**
	 * Deplace l'entité vers le Sud
	 */
	public boolean moveDown() {
		if (avatar.moving) {
			return false;
		}
		this.avatar.setOrientation("S");
		if (this.map.moveDown(this)) {
			this.avatar.setMoving(true);
			return true;
		}
		return false;
	}

	/**
	 * Deplace l'entité vers l'ouest
	 */
	public boolean moveLeft() {
		if (avatar.moving) {
			return false;
		}
		this.avatar.setOrientation("W");
		if(this.map.moveLeft(this)) {
			this.avatar.setMoving(true);
			return true;
		}
		return false;
	}

	/**
	 * Deplace l'entité vers l'est
	 */
	public boolean moveRight() {
		if (avatar.moving) {
			return false;
		}
		this.avatar.setOrientation("E");
		if(this.map.moveRight(this)) {
			this.avatar.setMoving(true);
			return true;
		}
		return false;
	}

	/**
	 * Taper l'entité vers le Nord
	 */
	public boolean hitUp() {
		return this.map.hitUp(this);
	}

	/**
	 * Taper l'entité vers le Sud
	 */
	public boolean hitDown() {
		return this.map.hitDown(this);
	}

	/**
	 * Taper l'entité vers l'ouest
	 */
	public boolean hitLeft() {
		return this.map.hitLeft(this);
	}

	/**
	 * Taper l'entité vers l'est
	 */
	public boolean hitRight() {
		return this.map.hitRight(this);
	}
	
	/**
	 * Tape sur place
	 */
	public boolean hitHere() {
		return this.map.hitHere(this);
	}

	/**
	 * Donne l'automate de l'entité
	 * 
	 * @return l'automate qui controle l'entité
	 */
	public AutomatonExe getAutomata() {
		return automata;
	}

	/**
	 * set l'automate de l'entité
	 * 
	 * @param l'automate qui controlera l'entité
	 */
	public void setAutomata(AutomatonExe automata) {
		this.automata = automata;
		this.currentState = automata.getinitialState();
	}

	/**
	 * Donne l'etat courant de l'automate de l'entité
	 * 
	 * @return l'etat de l'automate qui controle l'entité
	 */
	public StateExe getCurrentState() {
		return currentState;
	}	

	/**
	 * set l'etat courant de l'automate de l'entité
	 * 
	 * @param l'etat de l'automate qui controle l'entité
	 */
	public void setCurrentState(StateExe currentState) {
		this.currentState = currentState;
	}
	
	public void paintGround() {
		if(map.getCaseAt(getPosLine(), getPosCol()).getTypeCase() == TypeCase.NORMALGROUND) {
			if(this instanceof Player) {
				((NormalGround)map.getCaseAt(getPosLine(), getPosCol())).setMark(NormalGround.markCross);
			}else {
				((NormalGround)map.getCaseAt(getPosLine(), getPosCol())).setMark(NormalGround.markRound);
			}
			
		}
	}

	/**
	 * Fonction Pop des Entité appeler par les automates
	 * @param str les parametres recu par l'automate
	 */
	public abstract void pop(String str);

	/**
	 * Fonction Wizz des Entité appeler par les automates
	 * @param str les parametres recu par l'automate
	 */
	public abstract void wizz(String str);

	/**
	 * Deplace l'entité. Le déplacement peut varié en fct de l'entité
	 * 
	 * @param l ligne destination du déplacement
	 * @param c colonne destination du déplacement
	 */
	public abstract void move(int l, int c);
	
	/**
	 * Fonction qui retorune le Type de l'entité selon
	 * l'enumration TypeEntité 
	 * @return le type de l'entité
	 */
	public abstract int getTypeEntity();
	
	/**
	 * Fonction qui retourne la porté d'une entité
	 * @return la porté de l'entité
	 */
	public abstract int getRange();

	/**
	 * Fonction qui retourne la valeur maximal
	 * de vie de l'entité
	 * @return l'entier qui correspond à la valeur maximal de vie de l'entité 
	 */
	public abstract int getMaxHeath();
	
	/**
	 * Fonction qui retourne la valeur actuelle
	 * de vie de l'entité
	 * @return l'entier qui correspond à la valeur actuelle de vie de l'entité 
	 */
	public abstract int getCurrentHealth();
	
	/**
	 * Fonction qui permet de metrre a ajout la vie d'une entité
	 * elle ne doit pas être supérieur à la valeur maximal
	 * @param newHealth la nouvelle valeur de vie de l'entité
	 */
	public abstract void setCurrentHealth(int newHealth);
	
	/**
	 * Fonction qui retourne si l'entité est morte ou non
	 * @return true -> si l'entité est morte
	 * 		   false -> sinon
	 */
	public abstract boolean isDead();
	
	/**
	 * Fonction qui envoie l'entité dans la liste des entités a supprimer
	 */
	public abstract void deadEntity();
	
	/**
	 * Fonction qui retourne la valeur que l'entité inflige a une autre entité
	 * @return la valeur de dégat de l'entité
	 */
	public abstract int hit();

	/**
	 * Fonction qui indique que l'entité est attaquer
	 * @param e l'entité qui l'attaque
	 * @return le nombre de point de vie restant
	 */
	public abstract int isHit(Entity e);
	
	/**
	 * Fonction qui envoie le tick a son avatar
	 * @param elapsed le nombre de tick qui à passé
	 */
	public void tick(long elapsed) {
		// TODO Auto-generated method stub
		this.avatar.tick(elapsed);
	}
	
	/**
	 * Fonction qui détruit 
	 * @return
	 */
	public abstract boolean explode();


}
