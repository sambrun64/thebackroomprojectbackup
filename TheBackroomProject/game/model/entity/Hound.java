package model.entity;

import info3.game.Game;
import model.Map;
import vue.AvatarHound;

/**
 * Player : Représentation de l'entité Hound
 */
public class Hound extends Enemy {
	/**
	 * Création d'un Hound
	 * 
	 * @param l Ligne du Hound
	 * @param c Colonne du Hound
	 * @param m Map du Hound
	 */
	public Hound(int l, int c, Map m) {
		super(l, c, m);
		this.maxHealth = 50;
		this.currentHealth = maxHealth;
		this.avatar = new AvatarHound(this);

		switch (Game.difficulty) {
		case Game.EASY:
			this.damages = 25;
			break;
		case Game.HARD:
			this.damages = 40;
			break;
		case Game.NUKE:
			this.damages = 150;
			break;
		}
	}

	/**
	 * Déplace le Hound aux coordonnées indiqués
	 * 
	 * @param l Ligne destination du Hound
	 * @param c Colonne destination du Hound
	 */
	@Override
	public void move(int l, int c) {
		this.line = l;
		this.column = c;
	}

	@Override
	public void pop(String str) {
		switch(str) {
		
		case "N":
		case "F":
			this.moveUp();
			return;
		case "E":
		case "R":
			this.moveRight();
			return;
		case "W":
		case "L":
			this.moveLeft();
			return;
		case "S":
		case "B":
			this.moveDown();
			return;
		default:
			return;
		}
	}

	@Override
	public void wizz(String str) {
		switch(str) {
			case "N":
			case "F":
				this.hitUp();
				return;
			
			case "E":
			case "R":
				this.hitRight();
				return;
				
			case "W":
			case "L":
				this.hitLeft();
				return;
				
			case "S":
			case "B":
				this.hitDown();
				return;
		}
	}

	@Override
	public int getTypeEntity() {
		return TypeEntity.HOUND;
	}

	@Override
	public int getRange() {
		return 20;
	}

	
	public boolean isHit() {
		this.currentHealth = 0;
		this.isDead();
		return true;
	}

	@Override
	public int getMaxHeath() {
		return this.maxHealth;	}

	@Override
	public int getCurrentHealth() {
		return this.currentHealth;
	}

	@Override
	public void setCurrentHealth(int newHealth) {
		this.currentHealth = newHealth;
	}

	@Override
	public boolean isDead() {
		if(this.currentHealth <= 0) {
			this.deadEntity();
			return true;
		}
		return false;
	}

	@Override
	public void deadEntity() {
		this.getAutomata().getController().getGame().addRemoveEntities(this);
	}

	@Override
	public int hit() {
		return this.damages;
	}

	@Override
	public int isHit(Entity e) {
		return this.currentHealth -= e.damages;
	}

	@Override
	public boolean explode() {
		return false;
	}
}