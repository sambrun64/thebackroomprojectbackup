package model.entity;

import model.Case;
import model.Map;

import vue.AvatarShadow;
import model.loot.Loot;


public class Shadow extends Entity{
	private Player player;

	/**
	 * Constructeur de l'ombre
	 * Existe uniquement grace à un joueur
	 * @param c Case sur la laquel l'ombre va apparaitre
	 * @param m Carte du jeu
	 * @param p Joueur sur lequel l'ombre est relié
	 */
	public Shadow(Case c, Map m,Player p) {
		super(c.getLine(), c.getColumn(), m);
		this.player = p;
		this.avatar = new AvatarShadow(this);
		
		//verif case arrivé sans gameitem
		if(!c.hasGameItem()) {
			//split
			c.setGameItem(this);
			move(c.getLine(), c.getColumn());
		}else {
			if(c.getGameItem() instanceof Loot) {
				this.player.collectLoot(c);
				c.setGameItem(this);
				move(c.getLine(), c.getColumn());
			}
		}
	}
	
	/**
	 * Fonction d'autodestruction de l'ombre
	 */
	public void destroy() {
		player.destroyShadow();
	}

	@Override
	public void pop(String str) {
		switch(str) {
		
		case "N":
		case "F":
			super.moveUp();
			return;
		case "E":
		case "R":
			super.moveRight();
			return;
		case "W":
		case "L":
			super.moveLeft();
			return;
		case "S":
		case "B":
			super.moveDown();
			return;
		default:
			return;
		}
	}

	@Override
	public void wizz(String str) {
		// Rien a faire ici car l'ombre est simple
	}

	@Override
	public void move(int l, int c) {
		this.line = l;
		this.column = c;
	}

	@Override
	public int getTypeEntity() {
		return TypeEntity.OMBRE;
	}

	@Override
	public int getRange() {
		return 0; // 0 car l'ombre ne peut pas faire de dégat
	}

	@Override
	public int getMaxHeath() {
		return 0; // 0 car l'ombre n'a pas de point de vie
	}

	@Override
	public int getCurrentHealth() {
		return 0; // 0 car l'ombre n'a pas de point de vie
	}

	@Override
	public void setCurrentHealth(int newHealth) {
		// Rien ici car l'ombre n'a pas de point de vie
	}

	@Override
	public boolean isDead() {
		return false;
	}

	@Override
	public int hit() {
		return 0; // 0 car l'ombre ne peux pas mourir
	}

	@Override
	public void deadEntity() {
		// Rien car l'ombre ne peux pas mourir
	}

	@Override
	public int isHit(Entity e) {
		return 0; // 0 car l'ombre ne peux pas se faire attaquer
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public void tick(long elapsed) {
		avatar.tick(elapsed);
	}

	@Override
	public boolean explode() {
		return false;
	}

}
