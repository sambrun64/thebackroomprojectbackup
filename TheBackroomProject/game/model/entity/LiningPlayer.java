package model.entity;

import model.Case;
import model.TypeCase;
import vue.AvatarPlayer;

public class LiningPlayer {
	
	
	private Player player;
	private AvatarPlayer avatarplayer;

	/**
	 * Constrcuteur de la Doublure du joueur
	 * @param player
	 */
	public LiningPlayer(Player player) {
		this.player = player;
		this.avatarplayer = this.player.getAvatar();
	}
	
	/**
	 * Fonction qui effectue des changement au joueur selon l'environnement
	 * @return La vitesse d'animation nécessaire
	 */
	public int getLining() {
		
		Case c = player.getMap().getCaseAt(player.getPosLine(), player.getPosCol());
		
		switch (c.getTypeCase()) {
		case TypeCase.FIREGROUND:
			this.avatarplayer.setSprites(this.player.getAutomata().getController().getGame().sprites.getMcSpritesFire());
			this.avatarplayer.setIddle(this.player.getAutomata().getController().getGame().sprites.getIddleFire());
			this.player.setCurrentHealth(this.player.getCurrentHealth()-3);
			return AvatarPlayer.FAST_SPEED;
		case TypeCase.NORMALGROUND:
			this.avatarplayer.setSprites(this.player.getAutomata().getController().getGame().sprites.getMC());
			this.avatarplayer.setIddle(this.player.getAutomata().getController().getGame().sprites.getIddle());
			return AvatarPlayer.NORMAL_SPEED;
		case TypeCase.WATERGROUND:
			this.avatarplayer.setSprites(this.player.getAutomata().getController().getGame().sprites.getMcSpritesWater());
			if (this.player.getCurrentSatiety()>0) {
				this.player.setCurrentSatiety(this.player.getCurrentSatiety()-1);
			}
			this.avatarplayer.setIddle(this.player.getAutomata().getController().getGame().sprites.getIddleWater());
			return AvatarPlayer.SLOW_SPEED;
		}
		return AvatarPlayer.NORMAL_SPEED;
	}
	
	

}
