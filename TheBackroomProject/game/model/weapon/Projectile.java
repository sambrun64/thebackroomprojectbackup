package model.weapon;

import java.awt.Color;
import java.awt.Graphics;

import model.Map;
import model.entity.Entity;
import model.entity.TypeEntity;

import vue.AvatarBullet;

public class Projectile extends Entity{
	private int caseX;
	private int caseY;
	public float percentX;
	public float percentY;
	private float sin;
	private float cos;
	private int speed=25;
	private int range=50;
	private int viewportnbCol;
	private int viewportnbLine;
	private Map m;
	private int width;
	private int length;
	private float angle;
	
	/**
	 * 
	 * @param e
	 * @param cos
	 * @param sin
	 * @param angle
	 * @param width
	 * @param length
	 * @param m
	 * @param nbLine
	 * @param nbCol
	 * Constructeur de la classe Projectile
	 */
	public Projectile(Entity e,float cos, float sin, float angle,int width,int length,Map m,int nbLine,int nbCol) {
		super(e.getPosLine(),e.getPosLine(),m); 	// Position de la balle sur la map lors du tir
		this.avatar=new AvatarBullet(this);			// Création et attribution de l'avatar de la balle
		this.caseX=e.getPosCol();					// Coordonnées de la balle en case 
		this.caseY=e.getPosLine();
		this.range=e.getRange();					// Nombre de cases pouvants être parcouruent par la balle avant que cette dernière n'explose
		this.percentX=50;							// Densité de probabilité de la balle dans une case
		this.percentY=50;
		this.m=m;									// Attribution d'une map à la balle
		this.viewportnbCol=nbCol*2+1;				// Position de la balle dans le viewport
		this.viewportnbLine=nbLine*2+1;
		this.cos=cos;								// Attributs physiques de la balle pour les calculs
		this.sin=sin;
		this.angle=angle;
		this.width=width;
		this.length=length;
		this.damages=100;			
	}
	
	/**
	 * @param k
	 * @param l
	 * Move du projectile et calcul de la trajectoire en temps réel
	 */
	public void move(int k, int l) {
		this.calculAngle();
		int sizecaseX =this.width/this.viewportnbCol;		
		int sizecaseY =this.length/this.viewportnbLine;
		boolean hit=false;
		float x=100*((this.cos*this.speed)/sizecaseX);
		float y=100*((this.sin*this.speed)/sizecaseY);
		
		this.percentX+=x;
		this.percentY+=y;
		while((this.percentX>100||this.percentX<0||this.percentY>100||this.percentY<0)&& !hit) {
			if(Math.abs(this.percentX)>100&&!hit) {		// Test du changement de case en déplacement
				if(this.percentX<0) {
					caseX--;
					this.percentX=this.percentX+100;
				} else {
					caseX++;
					this.percentX=this.percentX-100;
				}
				range--;
			}
			if(this.percentX<0&&this.percentX>-100&&!hit) {
				this.percentX=100+this.percentX;
				caseX--;
			}
			
			caseX=utils.MathUtils.realModulo(this.caseX, m.getNbCol());
			if(Math.abs(this.percentY)>100&&!hit) {
				if(this.percentY<0) {
					caseY--;
					this.percentY=this.percentY+100;

				} else {
					caseY++;
					this.percentY=this.percentY-100;
				}
				range--;
			}
			if(this.percentY<0&&this.percentY>-100&&!hit) {
				this.percentY=100+this.percentY;
				caseY--;
			}
			caseY=utils.MathUtils.realModulo(this.caseY, m.getNbLine());	
		}
		if(range<0) {
			this.isDead();
		}
	}
	
	/**
	 * 
	 * @param g
	 * @param x
	 * @param y
	 * @param dx
	 * @param dy
	 * Affiche la balle à la bonne position
	 */
	public void paint(Graphics g,int x,int y,int dx,int dy) {
			g.setColor(Color.yellow);
			g.fillRect(x+(int)(dx*(percentX/100)), y+(int)(dy*(percentY/100)), 10, 10);
	}
	
	/**
	 * Récupère la colonne où se trouve l'entité
	 */
	public int getPosCol() {
		return this.caseX;
	}
	
	/**
	 * Récupère la ligne où se trouve l'entité
	 */
	public int getPosLine() {
		return this.caseY;
	}

	/**
	 * pop fait bouger l'entité et met à jour l'angle en vérifiant les 4 directions possibles avec des modulos PI
	 */
	@Override
	public void pop(String str) {
		switch(str) {
		
		case "N":
		case "F":
			this.move(0,0);
			return;
		case "E":
		case "R":
			this.angle=this.angle-(float)Math.PI/2;
			if(this.angle<-Math.PI) {
				this.angle=this.angle+2*(float)Math.PI;
			}
			this.move(0,0);
			return;
		case "W":
		case "L":
			this.angle=this.angle+(float)Math.PI/2;
			if(this.angle>Math.PI) {
				this.angle=this.angle-2*(float)Math.PI;
			}
			this.move(0,0);
			return;
		case "S":
		case "B":
			if(this.angle>0) {
				this.angle-=(float)Math.PI;
			} else {
				this.angle+=(float)Math.PI;
			}
			this.move(0,0);
			return;
		default:
			return;
		}
	}

	/**
	 * Vérifie l'impact de la balle
	 */
	@Override
	public void wizz(String str) {
			super.hitHere();
			this.isDead();
			return;
	}

	@Override
	public int getTypeEntity() {
		return TypeEntity.MISSILE;
	}

	/**
	 * Une balle meurt forcément à l'impact, isDead n'est appelé qu'à l'impact (Wizz)
	 */
	@Override
	public boolean isDead() {
		this.deadEntity();
		return true;
	}

	
	@Override
	public void deadEntity() {
		this.getAutomata().getController().getGame().addRemoveBullet(this);
	}

	@Override
	public int hit() {
		return this.damages;
	}

	@Override
	public void tick(long elapsed) {
		
		
	}
	
	/**
	 * Calcul l'angle de la balle lors de son parcours
	 */
	public void calculAngle() {
		this.cos=(float)Math.cos(this.angle);
		this.sin=-1*(float)Math.sin(this.angle);
	}

	/*
	 * Méthodes inutile pour balle
	 */
	@Override
	public int isHit(Entity e) {
		return 0;
	}
	@Override
	public int getRange() {
		return 0;
	}
	@Override
	public int getMaxHeath() {
		return 0;
	}
	@Override
	public int getCurrentHealth() {
		return 0;
	}
	@Override
	public void setCurrentHealth(int newHealth) {
	}

	/**
	 * La balle meurt après explosion
	 */
	@Override
	public boolean explode() {
		return this.isDead();
	}
}
