package model;

public class WaterGround extends Ground {
	/**
	 * Création d'une case d'eau
	 * 
	 * @param l Ligne de la case
	 * @param c Colonne de la case
	 */
	public WaterGround(int l, int c) {
		super(l, c);
		//isCrossable = true;
	}

	/**
	 * Donne le type d'une case d'eau
	 * 
	 * @return entier décrivant le type d'eau (liste des types dans TypeCase)
	 * @see TypeCase
	 */
	@Override
	public int getTypeCase() {
		return TypeCase.WATERGROUND;
	}
}
