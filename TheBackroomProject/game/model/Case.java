package model;

/**
 * Case : Représentation abstraite d'une case du jeu, sera instantiée par les
 * différents types de cases
 */
public abstract class Case {
	/* item / entité sur la carte */
	protected GameItem gameitem;
	/* position */
	protected int line;
	protected int column;
	/* Savoir si l'on peu traverser la case */
	protected boolean isCrossable;

	/**
	 * Création d'une case
	 * 
	 * @param l Ligne de la case
	 * @param c Colonne de la case
	 */
	public Case(int l, int c) {
		this.line = l;
		this.column = c;
	}

	/**
	 * Donne le type d'une case
	 * 
	 * @return entier décrivant le type de la case (liste des types dans TypeCase)
	 * @see TypeCase
	 */
	public abstract int getTypeCase();

	/**
	 * Set le gameItem présent sur la case
	 * 
	 * @param gm GameItem a ajouter sur la case
	 */
	public void setGameItem(GameItem gm) {
		// TODO plusieurs gameItem par case ?
		this.gameitem = gm;
	}

	/**
	 * Décrit la présence d'un gameItem sur la case
	 * 
	 * @return Un boolean a true si la case a un gameItem
	 */
	public boolean hasGameItem() {
		return this.gameitem != null;
	}

	/**
	 * Retourne le gameItem de la case
	 * 
	 * @return Le gameItem de la case
	 */
	public GameItem getGameItem() {
		return this.gameitem;
	}

	/**
	 * Supprime le gameItem de la case
	 */
	public void removeGameItem() {
		this.gameitem = null;
	}

	/**
	 * Donne la ligne de la case
	 * 
	 * @return La ligne de la case
	 */
	public int getLine() {
		return line;
	}

	/**
	 * Donne la colonne de la case
	 * 
	 * @return La colonne de la case
	 */
	public int getColumn() {
		return column;
	}
	
	public boolean getIsCrossable(){
		return isCrossable;
	}
}
