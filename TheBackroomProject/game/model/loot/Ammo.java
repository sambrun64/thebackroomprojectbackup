package model.loot;

import model.Map;

public class Ammo extends Loot{
	
	//set un loot ammo dans la map
	private int ammo;
	
	public Ammo(int l, int c, Map m) {
		super(l, c, m);
		this.ammo = 50;		// Stat pour les butins de munitions
	}
	
	/**
	 * Cette fonction renvoie la valeur de la variable munitions (ammo)
	 * pour les butins.
	 * 
	 * @return La variable ammo est renvoyée.
	 */
	public int getAmmo() {
		return this.ammo;
	}

	@Override
	/**
	 * Cette méthode renvoie le type de butin 'munition'.
	 * 
	 * @return entier type munition (liste des types dans TypeLoot)
	 * @see TypeLoot
	 */
	public int getTypeLoot() {
		return TypeLoot.AMMO;
	}
}
