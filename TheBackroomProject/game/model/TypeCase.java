package model;

/**
 * TypeCase : Description des différentes cases du jeu
 */
public class TypeCase {
	public static final int WALL = 0;
	public static final int NORMALGROUND = 1;
	public static final int WATERGROUND = 2;
	public static final int FIREGROUND = 3;
	public static final int PORTAL = 4;
}
