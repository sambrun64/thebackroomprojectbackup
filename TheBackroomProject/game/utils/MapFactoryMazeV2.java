package utils;

import java.util.Random;

import model.Case;
import model.FireGround;
import model.NormalGround;
import model.Portal;
import model.Wall;
import model.WaterGround;
import model.entity.Player;

public class MapFactoryMazeV2 {
	//	FLAG POUR LA MATRICE D'ENTIERS
	private static final int FLAG_SPAWNER = 6;
	private static final int FLAG_ROOM = 5;
	private static final int FLAG_PORTAL = 4;
	private static final int FLAG_WALL = 3;
	private static final int FLAG_PLAYER1 = 1;

	//	FLAG POUR INDIQUER LE BIOME PRINCIPAL
	private static final int FLAG_FIRE = 9;
	private static final int FLAG_NORMAL = 7;
	private static int MAIN_BIOME;

	//	FLAG POUR LA GESTION DES BIOMES
	private static final int FLAG_THIRD = 22;
	private static final int FLAG_SECOND = 21;
	private static final int FLAG_FIRST = 20;
	
	private static final int NB_BIOME = 3;
	private static final int BIOME_SIZE = 5;
	private static final float BIOME_PROB = 0.003F;

	private static int SIZE_COULOIR = 2;
	
	private static final float WALL_BREAK_PROB = 0.005F;
	private static final float ROOM_PROB = 0.001F;
	private static final int ROOM_SIZE = 5;
	
	/**
	 * Il crée un labyrinthe
	 * 
	 * @param nbLine       nombre de lignes de la carte
	 * @param nbCol        le nombre de colonnes
	 * @param size_couloir la taille des couloirs
	 * @param j1           le joueur
	 * @param rand         un générateur de nombres aléatoires
	 * @return Une matrice de Case.
	 */
	public static Case[][] initMap(int nbLine, int nbCol, int size_couloir, Player j1, Random rand) {
		SIZE_COULOIR = size_couloir;

		if (nbLine % (SIZE_COULOIR + 1) != 0 && nbCol % (SIZE_COULOIR + 1) != 0) {
			System.err.print("MapFactoryMaze: incorrect size");
			System.exit(1);
		}

		int[][] lab = init_labyrinth(nbLine, nbCol);
		Case[][] mapCase = new Case[nbLine][nbCol];

		MAIN_BIOME = rand.nextInt(NB_BIOME) + FLAG_NORMAL;

		int lineValue;
		int colValue;
		int Direction = -1;

		int countLanes = -1 * ((nbLine / (SIZE_COULOIR + 1)) * (nbCol / (SIZE_COULOIR + 1)) + 1);

		while (countLanes != -2) {

			lineValue = rand.nextInt((nbLine - 1));
			while (lineValue % (SIZE_COULOIR + 1) != 1) {
				lineValue = (lineValue + 1) % nbLine;
			}
			colValue = rand.nextInt((nbCol - 1));
			while (colValue % (SIZE_COULOIR + 1) != 1) {
				colValue = (colValue + 1) % nbCol;
			}

			Direction = rand.nextInt(3);
			if (countLanes == 60) {
				System.out.print(false);
			}

			switch (Direction) {
			case 0: // Right
				if (lab[(lineValue + (SIZE_COULOIR + 1)) % nbLine][colValue] != lab[lineValue][colValue]) {
					lab[(lineValue + SIZE_COULOIR) % nbLine][colValue] = lab[lineValue][colValue];
					for (int i = 1; i < SIZE_COULOIR; i++) {
						lab[(lineValue + SIZE_COULOIR) % nbLine][colValue + i] = FLAG_FIRST;
					}
					countLanes++;

					/*
					 * propager la valeur de la case choisie aux cases appartenant au couloir de la
					 * case qui jouxte cette première.
					 */
					propagation(lab, nbLine, nbCol, countLanes, (lineValue + (SIZE_COULOIR + 1)) % nbLine, colValue,
							lab[(lineValue + (SIZE_COULOIR + 1)) % nbLine][colValue], lab[lineValue][colValue]);
				}
				break;

			case 1: // Back
				if (lab[lineValue % nbLine][(colValue + (SIZE_COULOIR + 1)) % nbCol] != lab[lineValue][colValue]) {
					lab[lineValue % nbLine][(colValue + SIZE_COULOIR) % nbCol] = lab[lineValue][colValue];
					for (int i = 1; i < SIZE_COULOIR; i++) {
						lab[lineValue + i % nbLine][(colValue + SIZE_COULOIR) % nbCol] = FLAG_FIRST;
					}
					countLanes++;

					/*
					 * propager la valeur de la case choisie aux cases appartenant au couloir de la
					 * case qui jouxte cette première.
					 */
					propagation(lab, nbLine, nbCol, countLanes, lineValue, (colValue + (SIZE_COULOIR + 1)) % nbCol,
							lab[lineValue][(colValue + (SIZE_COULOIR + 1)) % nbCol], lab[lineValue][colValue]);

				}
				break;

			case 2: // Left
				if (lab[utils.MathUtils.realModulo(lineValue - (2), nbLine)][colValue] != lab[lineValue][colValue]) {
					lab[utils.MathUtils.realModulo(lineValue - 1, nbLine)][colValue] = lab[lineValue][colValue];
					for (int i = 1; i < SIZE_COULOIR; i++) {
						lab[utils.MathUtils.realModulo(lineValue - 1, nbLine)][(colValue + i) % nbCol] = FLAG_FIRST;
					}
					countLanes++;

					/*
					 * propager la valeur de la case choisie aux cases appartenant au couloir de la
					 * case qui jouxte cette première.
					 */
					propagation(lab, nbLine, nbCol, countLanes,
							utils.MathUtils.realModulo(lineValue - (SIZE_COULOIR + 1), nbLine), colValue,
							lab[utils.MathUtils.realModulo(lineValue - (SIZE_COULOIR + 1), nbLine)][colValue],
							lab[lineValue][colValue]);
				}
				break;

			case 3: // Front
				if (lab[lineValue][utils.MathUtils.realModulo(colValue - (2), nbCol)] != lab[lineValue][colValue]) {
					lab[lineValue][utils.MathUtils.realModulo(colValue - 1, nbCol)] = lab[lineValue][colValue];
					for (int i = 1; i < SIZE_COULOIR; i++) {
						lab[(lineValue + i) % nbLine][utils.MathUtils.realModulo(colValue - 1, nbCol)] = FLAG_FIRST;
					}
					countLanes++;

					/*
					 * propager la valeur de la case choisie aux cases appartenant au couloir de la
					 * case qui jouxte cette première.
					 */
					propagation(lab, nbLine, nbCol, countLanes, lineValue,
							utils.MathUtils.realModulo(colValue - (SIZE_COULOIR + 1), nbCol),
							lab[lineValue][utils.MathUtils.realModulo(colValue - (SIZE_COULOIR + 1), nbCol)],
							lab[lineValue][colValue]);
				}
				break;
			}
		}

		assignLanes(lab, nbLine, nbCol);
		createRooms(lab, nbLine, nbCol, rand);
		createMorePossibilities(lab, nbLine, nbCol, rand);
		for(int i = 1; i < NB_BIOME; i++) {
			createBiomes(lab, nbLine, nbCol, FLAG_FIRST+i, rand);
		}
		toCase(mapCase, lab, nbLine, nbCol, j1, rand);
		return mapCase;
	}

	/**
	 * Il propage la valeur d'une cellule à ses voisines
	 * 
	 * @param lab        matrice du labyrinth
	 * @param nbLine     la hauteur de la matrice
	 * @param nbCol      la largeur de la matrice
	 * @param countLanes le compteur de salles indépendantes
	 * @param lineValue  la coordonnée x de la case courant
	 * @param colValue   coordonnée y de la case courant
	 * @param oldVal     la valeur de la case avant la propagation
	 * @param newVal     la nouvelle valeur de la case
	 * 		_________________	__________________	__________________
	 *  	|X|X |X|X |X |X |	|X|X |X |X |X |X | 	|X|X |X |X |X |X |
	 *  	|X|-1|X|-3|-3|-3|	|X|-1|-1|-3|-3|-3| 	|X|-1|-1|-1|-1|-1|
	 *  	|X|X |X|X |X |X | ->|X|X |X |X |X |X |->|X|X |X |X |X |X | 
	 *  	|X|-4|X|-5|X |-6| 	|X|-4|X |-5|X |-6| 	|X|-4|X |-5|X |-6|
	 *  	|X|X |X|X |X |X | 	|X|X |X |X |X |X | 	|X|X |X |X |X |X |
	 *  	|X|-7|X|-8|X |-9| 	|X|-7|X |-8|X |-9| 	|X|-7|X |-8|X |-9|
	 */
	private static void propagation(int[][] lab, int nbLine, int nbCol, int countLanes, int lineValue, int colValue,
			int oldVal, int newVal) {
		if (oldVal > -1 || newVal > -1) {
			System.err.println("MapFactoryMaze.propagation: value problem");
			System.exit(1);
		}
		for (int x = 0; x < SIZE_COULOIR; x++) {
			for (int y = 0; y < SIZE_COULOIR; y++) {
				lab[lineValue + x][colValue + y] = newVal;
			}
		}

		if (lab[(lineValue + (SIZE_COULOIR + 1)) % nbLine][colValue % nbCol] != newVal
				&& lab[(lineValue + SIZE_COULOIR) % nbLine][colValue % nbCol] != FLAG_WALL) {
			lab[(lineValue + SIZE_COULOIR) % nbLine][colValue] = newVal;
			for (int i = 1; i < SIZE_COULOIR; i++) {
				lab[(lineValue + SIZE_COULOIR) % nbLine][colValue + i] = FLAG_FIRST;
			}

			/*
			 * propager la valeur de la case choisie aux cases appartenant au couloir de la
			 * case qui jouxte cette première.
			 */
			propagation(lab, nbLine, nbCol, countLanes, (lineValue + (SIZE_COULOIR + 1)) % nbLine, colValue, oldVal,
					newVal);
		}

		if (lab[lineValue][(colValue + (SIZE_COULOIR + 1)) % nbCol] != newVal
				&& lab[lineValue][(colValue + SIZE_COULOIR) % nbCol] != FLAG_WALL) {
			lab[lineValue][(colValue + SIZE_COULOIR) % nbCol] = newVal;
			for (int i = 1; i < SIZE_COULOIR; i++) {
				lab[lineValue + i][(colValue + SIZE_COULOIR) % nbCol] = FLAG_FIRST;
			}

			/*
			 * propager la valeur de la case choisie aux cases appartenant au couloir de la
			 * case qui jouxte cette première.
			 */
			propagation(lab, nbLine, nbCol, countLanes, lineValue, (colValue + (SIZE_COULOIR + 1)) % nbCol, oldVal,
					newVal);
		}

		if (lab[utils.MathUtils.realModulo(lineValue - (SIZE_COULOIR + 1), nbLine)][colValue] != newVal
				&& lab[utils.MathUtils.realModulo(lineValue - (1), nbLine)][colValue] != FLAG_WALL) {
			lab[utils.MathUtils.realModulo(lineValue - (1), nbLine)][colValue] = newVal;
			for (int i = 1; i < SIZE_COULOIR; i++) {
				lab[utils.MathUtils.realModulo(lineValue - (1), nbLine)][colValue + i] = FLAG_FIRST;
			}

			/*
			 * propager la valeur de la case choisie aux cases appartenant au couloir de la
			 * case qui jouxte cette première.
			 */
			propagation(lab, nbLine, nbCol, countLanes,
					utils.MathUtils.realModulo(lineValue - (SIZE_COULOIR + 1), nbLine), colValue, oldVal, newVal);
		}

		if (lab[lineValue][utils.MathUtils.realModulo(colValue - (SIZE_COULOIR + 1), nbCol)] != newVal
				&& lab[lineValue][utils.MathUtils.realModulo(colValue - (1), nbCol)] != FLAG_WALL) {
			lab[lineValue][utils.MathUtils.realModulo(colValue - (1), nbCol)] = newVal;
			for (int i = 1; i < SIZE_COULOIR; i++) {
				lab[lineValue + i][utils.MathUtils.realModulo(colValue - (1), nbCol)] = FLAG_FIRST;
			}

			/*
			 * propager la valeur de la case choisie aux cases appartenant au couloir de la
			 * case qui jouxte cette première.
			 */
			propagation(lab, nbLine, nbCol, countLanes, lineValue,
					utils.MathUtils.realModulo(colValue - (SIZE_COULOIR + 1), nbCol), oldVal, newVal);
		}
	}

	/**
	 * Il change la valeur des cellules qui font partie du même groupe pour leur
	 * affecter la valeur du FLAG_FIRST.
	 * 
	 * @param lab    matrice du labyrinth
	 * @param nbLine la hauteur de la matrice
	 * @param nbCol  la largeur de la matrice
	 * 		___________________		_____________
	 *  	|X |X |X |X |X |X |		|X|X|X|X|X|X|
	 *  	|X |-3|-3|-3|-3|-3|		|X|0|0|0|0|0|
	 *  	|X |X |X |-3|X |X | ->	|X|X|X|0|X|X|
	 *  	|-3|-3|X |-3|-3|-3| 	|0|0|X|0|0|0|
	 *  	|X |-3|X |X |X |-3| 	|X|0|X|X|X|0|
	 *  	|X |-3|-3|-3|X |-3| 	|X|0|0|0|X|0|
	 */
	private static void assignLanes(int[][] lab, int nbLine, int nbCol) {
		int[] tmp = new int[SIZE_COULOIR];
		for (int t = 0; t < tmp.length; t++) {
			tmp[t] = lab[1][t + 1];
		}
		for (int i = 0; i < nbLine; i++) {
			for (int j = 0; j < nbCol; j++) {
				for (int x = 0; x < tmp.length; x++) {
					if (lab[i][j] == tmp[x]) {
						lab[i][j] = FLAG_FIRST;
					}
				}
			}
		}
	}

	/**
	 * Il crée des salles dans le labyrinthe.
	 * 
	 * @param lab    matrice du labyrinth
	 * @param nbLine la hauteur de la matrice
	 * @param nbCol  la largeur de la matrice
	 * @param rand   nombre aléatoire
	 */
	private static void createRooms(int[][] lab, int nbLine, int nbCol, Random rand) {
		int countRoom = 0;
		int nbRoom = (int) ((nbLine * nbCol) * ROOM_PROB);
		nbRoom = Math.max(nbRoom, 3);
		while (countRoom < nbRoom) {
			int lineCoordRoom = rand.nextInt((nbLine - 1));
			int colCoordRoom = rand.nextInt((nbCol - 1));

			if (lab[lineCoordRoom][colCoordRoom] != FLAG_ROOM) {
				int height = rand.nextInt(ROOM_SIZE) + 2;
				int width = rand.nextInt(ROOM_SIZE) + 2;

				int tmpI, tmpJ;

				for (int i = -height; i <= height; i++) {
					for (int j = -width; j <= width; j++) {
						tmpI = utils.MathUtils.realModulo(lineCoordRoom + i, nbLine) % (nbLine - 1);
						tmpJ = utils.MathUtils.realModulo(colCoordRoom + j, nbCol) % (nbCol - 1);
						
						if (lab[tmpI][tmpJ] == FLAG_WALL) {
							lab[tmpI][tmpJ] = FLAG_FIRST;
						}
					}
				}

				if (countRoom == 0) {
					lab[lineCoordRoom][colCoordRoom] = FLAG_PORTAL;
				} else if (countRoom == 1) {
					lab[lineCoordRoom][colCoordRoom] = FLAG_PLAYER1;
				} else {
					lab[lineCoordRoom][colCoordRoom] = FLAG_ROOM;
				}
				countRoom++;
			}
		}
	}

	/**
	 * Il casse des murs de façon aléatoire dans le labyrinthe.
	 * 
	 * @param lab    matrice du labyrinth
	 * @param nbLine la hauteur de la matrice
	 * @param nbCol  la largeur de la matrice
	 * @param rand   nombre aléatoire
	 */
	private static void createMorePossibilities(int[][] lab, int nbLine, int nbCol, Random rand) {
		int countHole = 0;
		int nbHole = (int) ((nbLine * nbCol) * WALL_BREAK_PROB);
		while (countHole < nbHole) {
			int lineValue = rand.nextInt((nbLine - 1));
			int colValue = rand.nextInt((nbCol - 1));

			int coin = rand.nextInt(1);
			if (coin == 0) {
				if (lineValue % (SIZE_COULOIR + 1) != 0) {
					lineValue = (lineValue + 1) % nbLine;
				}
				if (colValue % (SIZE_COULOIR + 1) != 1) {
					colValue = (colValue + 1) % nbCol;
				}
			} else {
				if (colValue % (SIZE_COULOIR + 1) != 0) {
					colValue = (colValue + 1) % nbCol;
				}
				if (lineValue % (SIZE_COULOIR + 1) != 1) {
					lineValue = (lineValue + 1) % nbLine;
				}
			}

			if (lab[lineValue][colValue] == FLAG_WALL) {
				if (coin == 0) {
					for (int i = 1; i < (SIZE_COULOIR + 1); i++) {
						lab[(lineValue + i) % nbLine][colValue] = FLAG_FIRST;
					}
				} else {
					for (int i = 1; i < (SIZE_COULOIR + 1); i++) {
						lab[lineValue][(colValue + i) % nbCol] = FLAG_FIRST;
					}
				}
				countHole++;
			}
		}
	}

	/**
	 * Il crée des zones de biomes dans le labyrinthe.
	 * 
	 * @param lab    matrice du labyrinth
	 * @param nbLine la hauteur de la matrice
	 * @param nbCol  la largeur de la matrice
	 * @param biome  biome à affecter
	 * @param rand   nombre aléatoire
	 */
	private static void createBiomes(int[][] lab, int nbLine, int nbCol, int biome, Random rand) {
		int countBiomes = 0;
		
		int nbBiome = (int) ((nbLine * nbCol) * BIOME_PROB);
		nbBiome = Math.max(nbBiome, 3);
		while (countBiomes < nbBiome) {
			int lineCoordRoom = rand.nextInt((nbLine - 1));
			int colCoordRoom = rand.nextInt((nbCol - 1));

			//if (lab[lineCoordRoom][colCoordRoom] != FLAG_ROOM) {
				int height = rand.nextInt(BIOME_SIZE) + 2;
				int width = rand.nextInt(BIOME_SIZE) + 2;

				int tmpI, tmpJ;

				for (int i = -height; i <= height; i++) {
					for (int j = -width; j <= width; j++) {
						tmpI = utils.MathUtils.realModulo(lineCoordRoom + i, nbLine) % (nbLine - 1);
						tmpJ = utils.MathUtils.realModulo(colCoordRoom + j, nbCol) % (nbCol - 1);

						if (lab[tmpI][tmpJ] == FLAG_FIRST) {
							lab[tmpI][tmpJ] = biome;
						}
					}
				}
				countBiomes++;
			//}
		}
	}
	
	/**
	 * Il remplit la matrice de Case
	 * 
	 * @param map    matrice du labyrinth de cases
	 * @param lab    matrice du labyrinth d'entiers
	 * @param nbLine la hauteur de la matrice
	 * @param nbCol  la largeur de la matrice
	 * @param j1     le joueur
	 */
	private static void toCase(Case[][] map, int[][] lab, int nbLine, int nbCol, Player j1, Random rand) {
		for (int i = 0; i < nbLine - nbLine % SIZE_COULOIR; i += SIZE_COULOIR) {
			for (int j = 0; j < nbCol - nbCol % SIZE_COULOIR; j += SIZE_COULOIR) {
				for (int x = 0; x < SIZE_COULOIR; x++) {
					for (int y = 0; y < SIZE_COULOIR; y++) {
						convertToCase(lab[i + x][j + y], i + x, j + y, map, j1, rand);
					}
				}
			}
			
			/*
			 * Traite les dernières cases des lignes ayant des 
			 * coordonnées de colonnes comprises entre nbCol - nbCol % SIZE_COULOIR et nbCol
			 */
			for (int x = 0; x < SIZE_COULOIR; x++) {
				for (int y = nbCol - nbCol % SIZE_COULOIR; y < nbCol; y++) {
					convertToCase(lab[i + x][y], i + x, y, map, j1, rand);
				}
			}
			System.out.println("");
		}
		
		/*
		 * Traite les dernières lignes ayant des 
		 * coordonnées de lignes comprises entre nbLine - nbLine % SIZE_COULOIR et nbLine
		 */
		for (int x = nbLine - nbLine % SIZE_COULOIR; x < nbLine; x++) {
			for (int y = 0; y < nbCol; y++) {
				convertToCase(lab[x][y], x, y, map, j1, rand);
			}
		}
		System.out.println("");
	}

	/**
	 * Il convertit une valeur de la matrice d'entier en Case
	 * 
	 * @param val  la valeur de l'entier
	 * @param x    la coordonnée x de la case
	 * @param y    la coordonnée y de la case
	 * @param map  la carte à remplir
	 * @param j1   le joueur
	 * @param rand un générateur de nombres aléatoires
	 */
	private static void convertToCase(int val, int x, int y, Case[][] map, Player j1, Random rand) {
		switch (val) {
		case FLAG_WALL:
			map[x][y] = new Wall(x, y);
			break;
		case FLAG_PORTAL:
			map[x][y] = new Portal(x, y);
			break;
		case FLAG_SPAWNER:
		case FLAG_ROOM:
		case FLAG_FIRST:
			// Affecte la case du même type que le biome principal
			map[x][y] = affectCase(FLAG_FIRST, x, y);
			break;
		case FLAG_SECOND:
		case FLAG_THIRD:
			// Affecte la case avec un type différent du biome principal
			map[x][y] = affectCase(val, x, y);
			break;
		case FLAG_PLAYER1:
			map[x][y] = new NormalGround(x, y);
			map[x][y].setGameItem(j1);
			j1.move(x, y);
			break;
		default:
			System.err.println("MapFactoryMaze: flag unknown");
			System.exit(1);
			break;
		}
	}
	
	/**
	 * Il renvoie une nouvelle instance d'une classe qui étend la classe abstraite
	 * `Case` en fonction de la valeur des paramètres
	 * 
	 * @param locate l'emplacement du cas dans la matrice
	 * @param x      la coordonnée x de la case
	 * @param y      la coordonnée y de la case
	 * @return Un objet Cas.
	 */
	private static Case affectCase(int locate, int x, int y) {
		switch (MAIN_BIOME) {
		case FLAG_NORMAL:
			switch (locate) {
			case FLAG_FIRST:
				return new NormalGround(x, y);
			case FLAG_SECOND:
				return new FireGround(x, y);
			default:
				return new WaterGround(x, y);
			}
		case FLAG_FIRE:
			switch (locate) {
			case FLAG_FIRST:
				return new FireGround(x, y);
			case FLAG_SECOND:
				return new WaterGround(x, y);
			default:
				return new NormalGround(x, y);
			}
		default:
			switch (locate) {
			case FLAG_FIRST:
				return new WaterGround(x, y);
			case FLAG_SECOND:
				return new NormalGround(x, y);
			default:
				return new FireGround(x, y);
			}
		}
	}

	/**
	 * Il initialise le labyrinthe avec des murs sur les colonnes et lignes 
	 * dont le modulo de (SIZE_COULOIR+1) est égal à 0.
	 * 
	 * @param nbLine la hauteur de la matrice
	 * @param nbCol  la largeur de la matrice 
	 * 		________________
	 * 		|X|X |X|X |X|X |
	 * 		|X|-1|X|-2|X|-3| 		Représentation avec SIZE_COULOIR égale à 1
	 * 		|X|X |X|X |X|X | 
	 * 		|X|-4|X|-5|X|-6| 
	 *		|X|X |X|X |X|X |
	 * 
	 */
	private static int[][] init_labyrinth(int nbLine, int nbCol) {
		int[][] lab = new int[nbLine][nbCol];
		int cellValue = -1;
		
		for (int i = 0; i < nbLine; i++) {
			for (int j = 0; j < nbCol; j++) {
				lab[i][j] = FLAG_WALL;
			}
		}

		for (int i = 1; i < nbLine - nbLine % SIZE_COULOIR; i += SIZE_COULOIR + 1) {
			for (int j = 1; j < nbCol - nbCol % SIZE_COULOIR; j += SIZE_COULOIR + 1) {
				if ((i) % (SIZE_COULOIR + 1) != 0 && (j) % (SIZE_COULOIR + 1) != 0) {
					for (int x = 0; x < SIZE_COULOIR; x++) {
						for (int y = 0; y < SIZE_COULOIR; y++) {
							lab[i + x][j + y] = cellValue;
						}
					}
					cellValue--;
				}
			}
		}
		return lab;
	}
}
