package utils;

import java.awt.Font;
import java.awt.image.BufferedImage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class SpriteBank {
	private static HashMap<String, BufferedImage> sprites;
	private static Font gameFont16;
	private static Font gameFont24;
	private static Font gameFont32;
	private static Font gameFont36;
	private static Font gameFont48;
	
	private static BufferedImage whiteSprite = utils.ImgUtils.loadImg("resources/error.png");
	
	/**
	 * Charge dans la RAM tous les sprites du jeu au format .png.
	 * Toutes les images se trouvent dans le sous-arbre nommé "resources".
	 * 
	 * La fonction est, de préférence, appelée dès le lancement du programme.
	 * Ainsi, SpriteBank est la banque des sprites de Game.java qui contient tous les sprites
	 * chargés dans la RAM et accessible par toute la partie Model et View.
	 * Elle permet de ne pas avoir à charger chaque sprite depuis la ROM.
	 */
	public static void load() {
		sprites = new HashMap<String, BufferedImage>();
		ArrayList<String> paths = utils.Miscellaneous.parseFolder("resources", ".png");
		Iterator<String> it = paths.iterator();
		while (it.hasNext()) {
			String s = it.next();
			sprites.put(s, utils.ImgUtils.loadImg(s));
		}
		gameFont16 = new Font("resources/consola.ttf", Font.PLAIN, 16);
		gameFont24 = new Font("resources/consola.ttf", Font.PLAIN, 24);
		gameFont32 = new Font("resources/consola.ttf", Font.PLAIN, 32);
		gameFont36 = new Font("resources/consola.ttf", Font.PLAIN, 36);
		gameFont48 = new Font("resources/consola.ttf", Font.PLAIN, 48);
	}
	
	/**
	 * Retourne une image en fonction du chemin source où elle se situait dans la ROM
	 * @param filename
	 * @return
	 */
	public static BufferedImage getSprite(String filename) {
		try {
			return sprites.get(filename);
		} catch (Exception e) {
			return whiteSprite;
		}
	}
	
	/**
	 * Retourne la police Consola.ttf avec la taille indiquée.
	 * 
	 * Les tailles 16, 24, 32, 36, 48 sont créées en avance.
	 * Si la taille ne vaut pas 16, 24, 32, 36 ou 48, une nouvelle police doit être créée avant
	 * d'être retournée.
	 * @param size
	 * @return
	 */
	public static Font getFont(int size) {
		switch (size) {
		case 16:
			return gameFont16;
		case 24:
			return gameFont24;
		case 32:
			return gameFont32;
		case 36:
			return gameFont36;
		case 48:
			return gameFont48;
		default:
			return gameFont48.deriveFont(size);
		}
	}
}
