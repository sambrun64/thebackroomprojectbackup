package utils;

import java.util.Random;

import model.Case;
import model.NormalGround;
import model.Portal;
import model.TypeCase;
import model.Wall;
import model.entity.Player;

public class MapFactoryRand {

	public static final int WALL_PROB = 15;

	// TODO ENLEVER J2 POTENTIELLMENT
	/**
	 * Creer le tableau de case qui va bien en placant au hasard un pourcentage de
	 * murs
	 * 
	 * @param nbLine Nombre de lignes
	 * @param nbCol  Nombre de colonnes
	 * @param j1     joueur 1
	 * @param j2     joueur 2
	 * @param rand   Générateur aléatoire
	 * @return La matrice de cases
	 */
	public static Case[][] initMap(int nbLine, int nbCol, Player j1, Player j2, Random rand) {
		int nbWall = (nbLine * nbCol) * WALL_PROB / 100;
		Case[][] map = new Case[nbLine][nbCol];

		for (int i = 0; i < nbLine; i++) {
			for (int j = 0; j < nbCol; j++) {
				map[i][j] = new NormalGround(i, j);
			}
		}

		for (int n = 0; n < nbWall; n++) {
			int l = rand.nextInt(nbLine);
			int c = rand.nextInt(nbCol);
			map[l][c] = new Wall(l, c);
		}
		setPlayerPosition(map,nbLine,nbCol,rand);
		setPlayerPosition(j1, map, nbLine, nbCol, rand);
		setPlayerPosition(j2, map, nbLine, nbCol, rand);

		return map;
	}

	/**
	 * Place le joueurs comme il faut dans la map
	 * 
	 * @param j      joueur a placer
	 * @param map    tableau de cases
	 * @param nbLine Nombre de lignes
	 * @param nbCol  Nombre de colonnes
	 * @param rand   Générateur aléatoire
	 */
	private static void setPlayerPosition(Player j, Case[][] map, int nbLine, int nbCol, Random rand) {
		int c, l;
		do {
			l = rand.nextInt(nbLine);
			c = rand.nextInt(nbCol);
		} while (map[l][c].getTypeCase() != TypeCase.NORMALGROUND);
		map[l][c].setGameItem(j);
		j.move(l, c);
	}
	
	/**
	 * Place le portail comme il faut dans la map
	 * 
	 * @param map    tableau de cases
	 * @param nbLine Nombre de lignes
	 * @param nbCol  Nombre de colonnes
	 * @param rand   Générateur aléatoire
	 */
	private static void setPlayerPosition(Case[][] map, int nbLine, int nbCol, Random rand) {
		int c, l;
		do {
			l = rand.nextInt(nbLine);
			c = rand.nextInt(nbCol);
		} while (map[l][c].getTypeCase() != TypeCase.NORMALGROUND);
		map[l][c] = new Portal(l,c);
	}

}
