package vue;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Sprites {
	
	// TABLEAUX DE SPRITES ===============
	private BufferedImage[] cowboySprites;
	private BufferedImage[] mcSprites;
	private BufferedImage[] mcIddle;
	private BufferedImage[] mcIddleFire;
	private BufferedImage[] mcIddleWater;
	private BufferedImage[] shadowSprites;
	private BufferedImage[] mcSpritesFire;
	private BufferedImage[] mcSpritesWater;
	private BufferedImage[] zombieSprites;
	private BufferedImage[] slimeSprites;
	private BufferedImage[] houndSprites;

	// ===================================
	
	private final int SizeCompression = 128; 
	
	public Sprites() throws IOException{
		this.cowboySprites = resizeSprite(loadSprite("resources/winchester-4x6.png", 4, 6));
		this.mcSprites = resizeSprite(loadSprite("resources/sprites/mainCharacter.png", 1, 16));
		this.mcIddle = resizeSprite(loadSprite("resources/sprites/mainCharacterIddle.png", 1, 16));
		this.mcIddleFire = resizeSprite(loadSprite("resources/sprites/iddlePlayerFire.png", 1, 16));
		this.mcIddleWater = resizeSprite(loadSprite("resources/sprites/iddlePlayerWater.png", 1, 16));
		this.shadowSprites = resizeSprite(loadSprite("resources/sprites/shadowSprites.png", 1, 16));
		this.mcSpritesFire = resizeSprite(loadSprite("resources/sprites/SpritePlayerFire.png", 1, 16));
		this.mcSpritesWater = resizeSprite(loadSprite("resources/sprites/SpritePlayerWater.png", 1, 16));
		this.zombieSprites = resizeSprite(loadSprite("resources/sprites/zombie.png", 1, 16));
		this.slimeSprites = resizeSprite(loadSprite("resources/sprites/slime.png", 1, 16));
		this.houndSprites = resizeSprite(loadSprite("resources/sprites/hound.png", 1, 16));
	}
	
	public static BufferedImage[] loadSprite(String filename, int nrows, int ncols) throws IOException {
	    File imageFile = new File(filename);
	    if (imageFile.exists()) {
	      BufferedImage image = ImageIO.read(imageFile);
	      int width = image.getWidth(null) / ncols;
	      int height = image.getHeight(null) / nrows;

	      BufferedImage[] images = new BufferedImage[nrows * ncols];
	      for (int i = 0; i < nrows; i++) {
	        for (int j = 0; j < ncols; j++) {
	          int x = j * width;
	          int y = i * height;
	          images[(i * ncols) + j] = image.getSubimage(x, y, width, height);
	        }
	      }
	      return images;		//=====================================

	    }
	    return null;
	  }
	
	private BufferedImage[] resizeSprite(BufferedImage[] sprites) {
		
		BufferedImage[] tempArray = new BufferedImage[sprites.length];
		int count = 0;
		
		for (BufferedImage image : sprites) {
			BufferedImage temp = utils.ImgUtils.resizeImage(image, SizeCompression, SizeCompression);
			tempArray[count] = temp;
			count++;
		}
		
		return tempArray;
	}
	
	public BufferedImage[] getCowboy() {
		return this.cowboySprites;
	}
	
	public BufferedImage[] getMC() {
		return this.mcSprites;
	}
	
	public BufferedImage[] getIddle() {
		return this.mcIddle;
	}
	
	public BufferedImage[] getShadow() {
		return this.shadowSprites;
	}

	public BufferedImage[] getMcSpritesFire() {
		return mcSpritesFire;
	}

	public BufferedImage[] getMcSpritesWater() {
		return mcSpritesWater;
	}
	
	public BufferedImage[] getIddleFire() {
		return this.mcIddleFire;
	}
	
	public BufferedImage[] getIddleWater() {
		return this.mcIddleWater;
	}
	
	public BufferedImage[] getZombie() {
		return this.zombieSprites;
	}
	
	public BufferedImage[] getSlime() {
		return this.slimeSprites;
	}
	
	public BufferedImage[] getHound() {
		return this.houndSprites;
	}
}
