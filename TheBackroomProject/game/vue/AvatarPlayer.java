package vue;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import model.entity.Player;

public class AvatarPlayer extends AvatarEntity {
	
	public static final int SOUTH = 0;
	public static final int WEST = 4;
	public static final int EAST = 8;
	public static final int NORTH = 12;
	
	public static final int FAST_SPEED = 10;
	public static final int SLOW_SPEED = 110;
	public static final int NORMAL_SPEED = 60;
	
	private boolean hitted = false;	//Vrai si clignote car frappé

	public void setAnimationSpeed(int animationSpeed) {
		this.animMoveSpeed = animationSpeed;
	}

	int countFrame;	//Frames passées depuis le lancement de l'animation
	
	private Player player = null;
	private ViewController game = null;
	
	public void setSprites(BufferedImage[] sprites) {
		this.sprites = sprites;
	}
	
	public void setIddle(BufferedImage[] iddle) {
		this.iddle = iddle;
	}
	
	/**
	 * Vérifie que le sprite existe et l'initialise s'il ne l'est pas
	 */
	public void checkSprite() {
		if (this.sprites == null) {
			this.sprites = player.getAutomata().getController().getGame().sprites.getMC();
			this.iddle = player.getAutomata().getController().getGame().sprites.getIddle();
		}
	}
	
	// Appelée pour lier le modèle et la vue du joueur en jeu
	public AvatarPlayer(Player player) {
		this.player = player;
	}
	
	// Appelée pour afficher le joueur En Marche vers la droite dans le NextLevel
	public AvatarPlayer(ViewController game) {
		this.game = game;
		this.sprites = this.game.getGame().sprites.getMC();
	}
	
	public int getOrientation() {
		return this.orientation;	
	}
	
	/**
	 * Paint le sprite selectionné par m_imageIndex et découpe le 
	 * déplacement en indice selon le nombre de frames de l'animation
	 */
	public void paint(Graphics g, int x, int y, int dx, int dy) {
		if (this.player != null)
			this.checkSprite();
		
		BufferedImage printSprite = sprites[m_imageIndex];
		int temp_x = x;
		int temp_y = y;
		
		if (moving) {
			printSprite = sprites[m_imageIndex];
			switch (orientation) {
			case EAST:
				temp_x = x;
				break;
			case WEST:
				temp_x = x;
				break;
			case NORTH:
				temp_y = y;
				break;
			case SOUTH:
				temp_y = y ;
				break;
			}
		} else {
			printSprite = iddle[m_imageIndex];
		}
		
		//clignotement quand le joueur est touché
		if (!hitSwitch) {
			g.drawImage(printSprite, temp_x, temp_y, dx, dy, null);
		}
	}
	
	/**
	 * Réalisation de l'animation sur la base des ticks propagés à l'entité
	 * Maintiens à jour le m_imageIndex permettant le choix du sprite
	 * @param elapsed
	 */
	@Override
	public void tick(long elapsed) {
	if (this.sprites == null) {
		return;
	}
	
	//Clignottement quand touché
	timerHit(elapsed);
	
	//Gestion animation de mouvement
	if (moving) {
	    m_imageElapsed += elapsed;
	    if (m_imageElapsed > this.animMoveSpeed) {
	      m_imageElapsed = 0;
	      m_imageIndex = (m_imageIndex + 1) % sprites.length;
	      countFrame++;
	    }
	    
	    if (countFrame == NB_IMAGES) {
	    	if(this.player == null) {
	    		//Gestion de l'écran de transition
	    		this.setOrientation("E");
	    	}
	    	else {
	    		moving = false;
	    		m_imageIndex = orientation;
	    	}
    		countFrame = 0;
	    }
	  }	else {
		  //Gestion animation Iddle
		  m_imageElapsed += elapsed;
		    if (m_imageElapsed > 200) {
		      m_imageElapsed = 0;
		      m_imageIndex = (m_imageIndex + 1) % iddle.length;
		      countFrame++;
		    }
		    
		    if (countFrame == NB_IMAGES) {
		    	countFrame = 0;
		    	m_imageIndex = orientation;
		    }
	  }
	}
	
	public void setAnimMoveSpeed(int speed) {
		this.animMoveSpeed = speed;
	}
	
	//=====================================
	// CLIGNOTTEMENT QUAND TOUCHE
	
	/**
	 * Utilisé pour informer l'avatar que le joueur est touché et lancer le clignotement 
	 */
	public void isHit() {
		if (!this.hitted) {
			this.hitted = true;
			this.hitFrame = 0;
		}
	}
	
	private int hitFrame;
	private boolean hitSwitch = false;
	private long m_hitElapsed = 0;
	
	/**
	 * Animation du clignotement sur les ticks
	 * @param elapsed
	 */
	public void timerHit(long elapsed) {
		if (this.hitted) {
			if (hitSwitch) {
				hitSwitch = false;
			} else {
				hitSwitch = true;				
			}
			m_hitElapsed += elapsed;
		    if (m_hitElapsed > 200) {
		    	m_hitElapsed = 0;
		      hitFrame++;
		    }
		    
		    if (hitFrame == 4) {
		    	hitted = false;
		    	hitSwitch = false;
		    }
		}
	}
	//=====================================
}
