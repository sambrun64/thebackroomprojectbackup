package vue;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.image.BufferedImage;

import java.io.IOException;

import java.util.ArrayList;

import info3.game.Game;

import model.Case;
import model.Map;
import model.NormalGround;
import model.TypeCase;
import model.entity.Entity;
import model.entity.Player;
import model.entity.TypeEntity;
import model.loot.Armor;
import model.loot.Food;
import model.loot.Heart;
import model.loot.Ammo;
import model.loot.Loot;
import model.loot.TypeLoot;

import utils.MathUtils;

import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;

/*
 * Vue en jeu de la partie
 * */
public class ViewPortController {
	private final int viewNumber = 3;
	private final int SizeCompression = 64;
	
	/* Images des case */
	private BufferedImage normalGround;
	private BufferedImage fireGround;
	private BufferedImage[] fireGroundTab;
	private BufferedImage waterGround;
	private BufferedImage[] waterGroundTab;
	private BufferedImage portal;
	private BufferedImage[] portalTab;
	private BufferedImage groundMark;
	private BufferedImage groundMarkRound;
	private BufferedImage wall;
	private BufferedImage wallFull;
	
	/* selection de Loot */
	private BufferedImage heart;
	private BufferedImage meat;
	private BufferedImage armor;
	private BufferedImage ammo;
	private BufferedImage mental;

	/* selection de Weapon */
	private BufferedImage magnum;
	private BufferedImage shotgun;
	
	
	/* selection de la case */
	private BufferedImage selectCase;
	private BufferedImage[] selectCaseTab;
	private int imgCourant;
	

	private BufferedImage modeIcon;
	private BufferedImage[] modeIconTab;
	

	/* Variable du ViewPort */
	protected int viewPortSizeRayLine = 5;
	protected int viewPortSizeRayCol = 8;
	protected Player focusPlayer;

	private ViewPortMap vp;
	private ViewMiniMap vm;
	private ViewGlobalView vg;

	// private ArrayList<GameItem> list;

	private Game game;
	private Map m;
	private int pointOfView;
	
	private int tickCounter; 
	private int tickCaseCounter; 
	private final int timeAnim = 100;
	private final int timeAnimCase = 60;
	
	static Sprites sprites;
	
	private ArrayList<Entity> entitiesVP = new ArrayList<>();

	/**
	 * Crée la vue
	 * 
	 * @param g le jeu
	 * @param m la map
	 */
	public ViewPortController(Game g, Map m) {
		initSprites();
		this.game = g;
		this.focusPlayer = m.getJ1();
		this.pointOfView = 0;
		this.m = m;
	}

	/**
	 * la taille du vp
	 * 
	 * @return la taille du vp en ligne
	 */
	public int getViewPortSizeRayLine() {
		return this.viewPortSizeRayLine;
	}

	/**
	 * la taille du vp
	 * 
	 * @return la taille du vp en col
	 */
	public int getViewPortSizeRayCol() {
		return this.viewPortSizeRayCol;
	}

	/**
	 * le joeuur focus
	 * 
	 * @return le joueur focus
	 */
	public Player getFocusPlayer() {
		return this.focusPlayer;
	}

	/**
	 * set la map 
	 * 
	 * @param m la nouvelle map a afficher
	 */
	public void setMap(Map m) {
		this.focusPlayer = m.getJ1();

		//set sur les différentes versions d'affichage
		this.vp = new ViewPortMap(this, m);
		this.vm = new ViewMiniMap(this, m);
		this.vg = new ViewGlobalView(this, m);
		this.m = m;
	}

	/**
	 * charge tous les sprites
	 */
	private void initSprites() {
		this.wall = utils.ImgUtils.loadImg("resources/ImagesCases/CaseMur1.png");
		this.wallFull = utils.ImgUtils.loadImg("resources/ImagesCases/CaseMurFull.png");
		this.normalGround = utils.ImgUtils.loadImg("resources/ImagesCases/CaseSol.png");
		this.groundMark = utils.ImgUtils.loadImg("resources/ImagesCases/CaseSolMarque.png");
		this.groundMarkRound = utils.ImgUtils.loadImg("resources/ImagesCases/CaseSolMarqueRond.png");
		this.waterGround = utils.ImgUtils.loadImg("resources/ImagesCases/CaseEau.png");
		this.fireGround = utils.ImgUtils.loadImg("resources/ImagesCases/CaseFeu.png");
		
		//init sprite Loot
		this.heart=utils.ImgUtils.loadImg("resources/ImagesLoot/heart.png");
		this.meat=utils.ImgUtils.loadImg("resources/ImagesLoot/meat.png");
		this.armor=utils.ImgUtils.loadImg("resources/ImagesLoot/armor.png");
		
		this.ammo=utils.ImgUtils.loadImg("resources/ImagesLoot/ammo.png");

		this.mental=utils.ImgUtils.loadImg("resources/SanteMentale.png");

		//init sprite Weapon
		this.magnum=utils.ImgUtils.loadImg("resources/ImagesWeapon/magnum.png");
		this.shotgun=utils.ImgUtils.loadImg("resources/ImagesWeapon/shotgun.png");
		
		this.portalTab = utils.ImgUtils.loadSprite("resources/ImagesCases/PortailSprite.png", 1, 3);
		this.waterGroundTab = utils.ImgUtils.loadSprite("resources/ImagesCases/CaseEauSprite.png", 1, 4);
		this.fireGroundTab = utils.ImgUtils.loadSprite("resources/ImagesCases/CaseFeuSprite.png", 1, 4);

		//cht du tableau de sprite de select case
		this.selectCaseTab = utils.ImgUtils.loadSprite("resources/ImagesCases/CaseSelectSprite.png",1,3);
		//set de la case courante
		this.selectCase = this.selectCaseTab[0];
		
		//cht du tableau de sprite de mode symbol
		this.modeIconTab = utils.ImgUtils.loadSprite("resources/imageTemp/ModeSprite.png",1,3);
		//set de la case courante
		this.modeIcon = this.modeIconTab[0];
		resizeSprite();
	}

	/**
	 * resize les sprites 
	 */
	private void resizeSprite() {
		this.wall = utils.ImgUtils.resizeImage(this.wall, SizeCompression, SizeCompression);
		this.wallFull = utils.ImgUtils.resizeImage(this.wallFull, SizeCompression, SizeCompression);
		this.portal = utils.ImgUtils.resizeImage(this.portal, SizeCompression, SizeCompression);
		this.normalGround = utils.ImgUtils.resizeImage(this.normalGround, SizeCompression, SizeCompression);
		this.groundMark = utils.ImgUtils.resizeImage(this.groundMark, SizeCompression, SizeCompression);
		this.groundMarkRound = utils.ImgUtils.resizeImage(this.groundMarkRound, SizeCompression, SizeCompression);
		
		this.heart=utils.ImgUtils.resizeImage(this.heart, SizeCompression, SizeCompression);
		this.meat=utils.ImgUtils.resizeImage(this.meat, SizeCompression, SizeCompression);
		this.armor=utils.ImgUtils.resizeImage(this.armor, SizeCompression, SizeCompression);
		
		this.ammo=utils.ImgUtils.resizeImage(this.ammo, SizeCompression, SizeCompression);
		
		this.mental=utils.ImgUtils.resizeImage(this.mental, SizeCompression, SizeCompression);
		
		this.magnum=utils.ImgUtils.resizeImage(this.magnum, SizeCompression, SizeCompression);
		this.shotgun=utils.ImgUtils.resizeImage(this.shotgun, SizeCompression, SizeCompression);
		
		this.selectCaseTab[0] = utils.ImgUtils.resizeImage(this.selectCaseTab[0], SizeCompression, SizeCompression);
		this.selectCaseTab[1] = utils.ImgUtils.resizeImage(this.selectCaseTab[1], SizeCompression, SizeCompression);
		this.selectCaseTab[2] = utils.ImgUtils.resizeImage(this.selectCaseTab[2], SizeCompression, SizeCompression);
		
		for(int i = 0; i < this.fireGroundTab.length; i++) {
			this.fireGroundTab[i] = utils.ImgUtils.resizeImage(this.fireGroundTab[i], SizeCompression, SizeCompression);
			this.waterGroundTab[i] = utils.ImgUtils.resizeImage(this.waterGroundTab[i], SizeCompression, SizeCompression);
		}
		for(int i = 0; i < this.portalTab.length; i++) {
			this.portalTab[i] = utils.ImgUtils.resizeImage(this.portalTab[i], SizeCompression, SizeCompression);
		}
	}

	/**
	 * tick
	 * 
	 * @param elapsed temps passé entre 2 ticks
	 */
	public void tick(long elapsed) {
		
		if(this.tickCounter > this.timeAnim) {
			imgCourant = (imgCourant + 1)%this.selectCaseTab.length;
			
			this.selectCase = this.selectCaseTab[imgCourant];
			this.tickCounter = 0;
		}
		this.tickCounter++;
		
		if(this.tickCaseCounter > this.timeAnimCase) {
			imgCourant = (imgCourant + 1)%this.fireGroundTab.length;
			this.fireGround = this.fireGroundTab[imgCourant];
			this.waterGround = this.waterGroundTab[imgCourant];
			imgCourant = (imgCourant + 1)%this.portalTab.length;
			this.portal = this.portalTab[imgCourant];
			this.tickCaseCounter = 0;
		}
		this.tickCaseCounter++;
	}

	/**
	 * paint
	 * 
	 * @param g la toile
	 * @param sizeX la taille en X
	 * @param sizeY la taille en Y
	 */
	public void paint(Graphics g, int sizeX, int sizeY) {
		float k, m;
		k = 0;
		m = 0;
		AvatarPlayer avatar = this.focusPlayer.getAvatar();
		if (avatar.moving) {
			float pas = (float)avatar.countFrame / (float)avatar.NB_IMAGES;
			int direction = avatar.getOrientation();
			switch (direction) {
			case AvatarPlayer.WEST:
				k = -1+pas;
				break;
			case AvatarPlayer.NORTH:
				m = -1+pas;
				break;
			case AvatarPlayer.EAST:
				k = 1-pas;
				break;
			default:
				m = 1-pas;
				break;
			}
		}
		switch (this.pointOfView) {
		case 0:
			this.vp.paint(g, sizeX, sizeY, k, m);
			break;
		case 1:
			this.vm.paint(g, sizeX, sizeY);
			break;
		case 2:
			this.vg.paint(g, sizeX, sizeY);
			break;
		default:
			break;
		}
	}
	
	/**
	 * paint
	 * 
	 * @param g la toile
	 * @param c la case
	 * @param x la position ou la paint
	 * @param y la position ou la paint
	 * @param dx la taille en x
	 * @param dy la taille en y
	 */
	protected void paintCase(Graphics g, Case c, int x, int y, int dx, int dy) {
		BufferedImage printSprite = null;
		if (c.hasGameItem() && c.getGameItem() instanceof Entity) {
			entitiesVP.add((Entity) c.getGameItem());
		}
		switch (c.getTypeCase()) {
		case TypeCase.WALL:
			//mur plein
			if (m.getCaseAt(utils.MathUtils.realModulo(c.getLine() + 1, m.getNbLine()), c.getColumn())
					.getTypeCase() == TypeCase.WALL) {
				printSprite = wallFull;
			} else {
				printSprite = wall;
			}
			break;
		case TypeCase.PORTAL:
			printSprite = portal;
			break;
		case TypeCase.NORMALGROUND:
			NormalGround tmp = (NormalGround)c;
			if(tmp.getMark()== NormalGround.markCross) {
				printSprite = groundMark;
			}else if(tmp.getMark()== NormalGround.markRound){
				printSprite = groundMarkRound;
			}else {
				printSprite = normalGround;
			}
			break;
		case TypeCase.FIREGROUND:
			printSprite = fireGround;
			break;

		case TypeCase.WATERGROUND:
			printSprite = waterGround;
			break;
		default:
			System.err.println("Aucun sprite associé !");
			System.exit(1);
		}
		g.drawImage(printSprite, x, y, dx, dy, null);
		
		// affichage des Loot par dessus les cases floors
		if (c.hasGameItem() && c.getGameItem() instanceof Loot) {
			switch (((Loot) c.getGameItem()).getTypeLoot()) {
			case TypeLoot.FOOD:
				g.drawImage(this.meat, x, y, dx, dy, null);
				break;
			case TypeLoot.HEART:
				g.drawImage(this.heart, x, y, dx, dy, null);
				break;
			case TypeLoot.AMMO:
				g.drawImage(this.ammo, x, y, dx, dy, null);
				break;
			case TypeLoot.ARMOR:
				g.drawImage(this.armor, x, y, dx, dy, null);
				break;
			}
		}
	}

	/**
	 * colorie une case en une couleur
	 * 
	 * @param g la toile
	 * @param c couleur 
	 * @param x la position ou la paint
	 * @param y la position ou la paint
	 * @param dx la taille en x
	 * @param dy la taille en y
	 */
	protected void colorCase(Graphics g, Color c, int x, int y, int dx, int dy) {
		g.setColor(c);
		g.fillRect(x, y, dx, dy);
	}

	/**
	 * paint les entitées
	 * 
	 * @param g la toile
	 * @param dx la taille en x
	 * @param dy la taille en y
	 * @param k decalage 
	 * @param n decalage 
	 */
	protected void paintEntities(Graphics g, int dx, int dy,float k, float n) {
		int x;
		int y;
		if (this.pointOfView == 0) {

			ArrayList<Case> liste = vp.getListe();
			int orig_line = liste.get(20).getLine();
			int orig_col = liste.get(20).getColumn();

			for (Entity ent : entitiesVP) {
				if (ent instanceof Player) {
					x = MathUtils.realModulo(ent.getPosCol() - orig_col, m.getNbCol()) * dx;
					y = MathUtils.realModulo(ent.getPosLine() - orig_line, m.getNbLine()) * dy;
					((Entity) ent).askForPaint(g, x, y, dx, dy);
				}
				else {
					x = MathUtils.realModulo(ent.getPosCol() - orig_col, m.getNbCol());
					y = MathUtils.realModulo(ent.getPosLine() - orig_line, m.getNbLine());
					((Entity) ent).askForPaint(g,(int)((x+k)* dx), (int)((y+n)*dy), dx, dy);
				}

			}
		} else {
			for (Entity ent : entitiesVP) {
				x = MathUtils.realModulo(ent.getPosCol(), m.getNbCol()) * dx;
				y = MathUtils.realModulo(ent.getPosLine(), m.getNbLine()) * dy;
				((Entity) ent).askForPaint(g, x, y, dx, dy);
			}
		}
		entitiesVP.clear();
	}

	/**
	 * paint les balles 
	 * 
	 * @param g la toile
	 * @param dx la taille en x
	 * @param dy la taille en y
	 * @param k decalage 
	 * @param n decalage 
	 */
	public void paintBullet(Graphics g, int dx, int dy,float k,float n) {
		int x;
		int y;
		for (Entity bullet : game.getProjectile()) {
			ArrayList<Case> liste = vp.getListe();
			int orig_line = liste.get(20).getLine();
			int orig_col = liste.get(20).getColumn();

			if (this.pointOfView == 0) {
				x = MathUtils.realModulo(bullet.getPosCol() - orig_col, m.getNbCol());
				y = MathUtils.realModulo(bullet.getPosLine() - orig_line, m.getNbLine()) ;
				((Entity) bullet).askForPaint(g,(int)((x+k)* dx), (int)((y+n)*dy), dx, dy);
			} else {
				x = MathUtils.realModulo(bullet.getPosCol(), m.getNbCol()) * dx;
				y = MathUtils.realModulo(bullet.getPosLine(), m.getNbLine()) * dy;
				((Entity) bullet).askForPaint(g, x, y, dx, dy);
			}
		}
	}
	
	/**
	 * affiche l'interface
	 * 
	 * @param g la toile
	 * @param player le joueur dont on veut afficher les infos
	 */
	public void paintInterface(Graphics g,Player player) {
		// affichage de la barre de vie de player
		g.drawImage(this.heart, 20, 20, 30, 30, null);
		g.setColor(new Color(0,0,0,120));
		g.fillRect(60, 20, 200, 30);
		g.setColor(new Color(217, 1, 21, 190));
		g.fillRect(60, 20, (int)((float)(200*player.getCurrentHealth()/player.getMaxHeath())), 30);

		//affichage de la barre de satiété
		g.drawImage(this.meat, 20, 70, 30, 30, null);
		g.setColor(new Color(0,0,0,120));
		g.fillRect(60, 70, 200, 30);
		g.setColor(new Color(220,118,51,190));
		g.fillRect(60, 70, (int)((float)(200*player.getCurrentSatiety())/player.getMaxSatiety()), 30);
		
		//affichage de la barre d'armure
		g.drawImage(this.armor, 800, 20, 30, 30, null);
		g.setColor(new Color(0,0,0,120));
		g.fillRect(850, 20, 200, 30);
		g.setColor(new Color(75, 0, 130, 250));
		g.fillRect(850, 20, (int)((float)(200*player.getCurrentArmor()/player.getMaxArmor())), 30);
		
		//affichage de la santé mentale
		g.drawImage(this.mental, 20, 120, 30, 30, null);
		g.setColor(new Color(0,0,0,120));
		g.fillRect(60, 120, 200, 30);
		g.setColor(new Color(220,118,51,190));
		g.fillRect(60, 120, (int)((float)(200*player.getCurrentMentalHealth())/player.getMaxMentalHealth()), 30);

		//affichage arme sélectionner
		if(this.game.getWeapon().getMode()==0) {
			g.drawImage(this.magnum, 20, 150, 80,80, null);
		}else {
			g.drawImage(this.shotgun, 20, 150, 80, 80, null);
		}

		//affichage de le compteur de munitions
		g.drawImage(this.ammo, 100, 170, 30, 30, null);
		g.setColor(new Color(0,0,0,255));
		g.drawString(String.valueOf(this.focusPlayer.getCurrentAmmo()), 130, 190);
		
		//mode de jeu
		if(this.focusPlayer.getState() == Player.fusion) {
			this.modeIcon = this.modeIconTab[0];
		} else if(this.focusPlayer.getState() == Player.split) {
			this.modeIcon = this.modeIconTab[1];
		} else {
			this.modeIcon = this.modeIconTab[2];
		}
		g.drawImage(this.modeIcon, 180, 170, 50, 50, null);
	}
	
	
	/**
	 * Met a jour la vu du jeu (déclanché par l'appuie sur 'm')
	 */
	public void switchMode() {
		this.pointOfView = (this.pointOfView + 1) % viewNumber;
		System.out.println("" + pointOfView);
	}

	/**
	 *  position sourie au mouvement, pas toutes les vues en ont besoins
	 * 
	 * @param x position sourie
	 * @param y  position sourie
	 */
	public void mouseMove(int x, int y) {
		switch (this.pointOfView) {
		case 0:
			this.vp.mouseMove(x, y);
			break;
		case 1:
			// this.vm.paint(g, sizeX, sizeY);
			break;
		case 2:
			// this.vg.paint(g, sizeX, sizeY);
			break;
		default:
			break;
		}
	}

	/**
	 *  pas utile
	 *  
	 * @param x position sourie
	 * @param y  position sourie
	 */
	public void clickAs(int x, int y) {}

	/**
	 * endroit du release
	 *  
	 * @param x position sourie
	 * @param y  position sourie
	 */
	public void clickReleaseAs(int x, int y, int button) {
		switch (this.pointOfView) {
		case 0:
			this.vp.clickReleaseAs(x,y,button);

			break;
		case 1:
			// this.vm.paint(g, sizeX, sizeY);
			break;
		case 2:
			// this.vg.paint(g, sizeX, sizeY);
			break;
		default:
			break;
		}
	}
	
	/**
	 * paint les infos d'une case
	 *  
	 * @param g la toile
	 * @param c couleur 
	 * @param x la position ou la paint
	 * @param y la position ou la paint
	 * @param dx la taille en x
	 * @param dy la taille en y
	 */
	private void paintInfoCase(Graphics g, Case c, int x, int y, int dx, int dy) {
		/* Affichage des infos de la case */
		g.setColor(new Color(32,248,22,127));
		g.fillRoundRect(x, y, 2*dx, dy,10,10);
		g.setColor(new Color(0,0,0,255));
		g.drawRoundRect(x, y, 2*dx, dy,10,10);
		//food
		if(c.getGameItem() instanceof Loot) {
			if(c.getGameItem() instanceof Food) {
				g.drawString("Gigot",x+10,y+12);
				g.drawString("MiamCase: "+((Food)c.getGameItem()).getFood(),x+5,y+26);
			}else if(c.getGameItem() instanceof Heart) {
				g.drawString("Vie",x+10,y+12);
				g.drawString("LifeCase: "+((Heart)c.getGameItem()).getLife(),x+5,y+26);
			}else if(c.getGameItem() instanceof Armor) {
				g.drawString("Armure",x+10,y+12);
				g.drawString("ArmorCase: "+((Armor)c.getGameItem()).getArmor(),x+5,y+26);
			} else {
				g.drawString("Munitions",x+10,y+12);
				g.drawString("AmmoCase: "+((Ammo)c.getGameItem()).getAmmo(),x+5,y+26);
			}
			//entité
		}else {
			Entity entity = (Entity)c.getGameItem();
			switch(entity.getTypeEntity()) {
			case TypeEntity.JFUSION:
				g.drawString("J1 & J2",x+10,y+12);
				g.drawString("PV: "+entity.getCurrentHealth()+"/"+entity.getMaxHeath(),x+5,y+26);
				g.drawString("Dégats: "+entity.hit(),x+5,y+40);
				g.drawString("Automate: "+entity.getAutomata().getName(),x+5,y+54);
				break;
			case TypeEntity.OMBRE:
				g.drawString("J2",x+10,y+12);
				g.drawString("PV: "+entity.getCurrentHealth()+"/"+entity.getMaxHeath(),x+5,y+26);
				g.drawString("Automate: "+entity.getAutomata().getName(),x+5,y+40);
				break;
			case TypeEntity.PLAYER1:
				g.drawString("J1",x+10,y+12);
				g.drawString("PV: "+entity.getCurrentHealth()+"/"+entity.getMaxHeath(),x+5,y+26);
				g.drawString("Dégats: "+entity.hit(),x+5,y+40);
				g.drawString("Automate: "+entity.getAutomata().getName(),x+5,y+54);
				break;
			case TypeEntity.SLIME:
				g.drawString("Voltaire",x+10,y+12);
				g.drawString("PV: "+entity.getCurrentHealth()+"/"+entity.getMaxHeath(),x+5,y+26);
				g.drawString("Dégats: "+entity.hit(),x+5,y+40);
				g.drawString("Automate: "+entity.getAutomata().getName(),x+5,y+54);
				break;
			case TypeEntity.ZOMBIE:
				g.drawString("Théophile",x+10,y+12);
				g.drawString("PV: "+entity.getCurrentHealth()+"/"+entity.getMaxHeath(),x+5,y+26);
				g.drawString("Dégats: "+entity.hit(),x+5,y+40);
				g.drawString("Automate: "+entity.getAutomata().getName(),x+5,y+54);
				break;
			case TypeEntity.HOUND:
				g.drawString("Christine",x+10,y+12);
				g.drawString("PV: "+entity.getCurrentHealth()+"/"+entity.getMaxHeath(),x+5,y+26);
				g.drawString("Dégats: "+entity.hit(),x+5,y+40);
				g.drawString("Automate: "+entity.getAutomata().getName(),x+5,y+54);
				break;
			}
		}
	}

	/**
	 * paint les infos d'une case
	 *  
	 * @param g la toile
	 * @param c couleur 
	 * @param x la position ou la paint
	 * @param y la position ou la paint
	 * @param dx la taille en x
	 * @param dy la taille en y
	 * @param larg largeur du graphique 
	 * @param haut hauteur du graphique 
	 */
	public void brightCase(Graphics g, Case c, int x, int y, int dx, int dy,int larg,int haut) {
		/* carré select case */
		g.drawImage(this.selectCase, x, y, dx, dy, null);
		
		if(c.hasGameItem()){
			int tmp;
			if(x-dx/2 < 0) {
				tmp = 0;
			}else if(x+dx/2+dx > larg){
				tmp = x-dx;
			}else {
				tmp = x-dx/2;
			}
			paintInfoCase(g, c, tmp,  (y-dy<0)?y+dy:y-dy, dx, dy);
		}
	}

	/**
	 * tire un coups de feu
	 * 
	 * @param mouseX pos en x du shoot
	 * @param mouseY pos en y du shoot
	 * @param c case actuel
	 * @param nbLine taille en ligne 
	 * @param nbCol taille en colonnes 
	 */
	public void shot(int mouseX,int mouseY,int button,Case c,int nbLine,int nbCol) {
		game.shot(focusPlayer,mouseX,mouseY,button,c,nbLine,nbCol);

	}
	
	/**
	 * affiche la santé mentale
	 * 
	 * @param g la toile
	 * @param l largeur
	 * @param h hauteur
	 */
	public void paintMentalHealth(Graphics g,int l,int h) {
		//si la santé mental est pas max
		if(m.getJ1().getCurrentMentalHealth() != m.getJ1().getMaxMentalHealth()) {
			g.setColor(Color.BLACK);
			//calcul du rayon de l'elipse
			int rx = Math.max(m.getJ1().getCurrentMentalHealth(), 250);
			rx = (int)(1.5*l*rx/m.getJ1().getMaxMentalHealth());
			int ry = rx;
			
			Area a1 = new Area(new Rectangle2D.Double(0, 0, l, h));
			Area a2 = new Area(new Ellipse2D.Double(l/2 - rx/2, h/2 - ry/2, rx, ry));        
	
			a1.subtract(a2);
			((Graphics2D) g).fill(a1);
		}
	}
	
}
