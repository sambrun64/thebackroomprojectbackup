package vue;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import model.entity.Shadow;

public class AvatarShadow extends AvatarEntity{
	
	Shadow shadow;
	
	/**
	 * Vérifie que le sprite existe et l'initialise s'il ne l'est pas
	 */
	@Override
	public void checkSprite() {
		if (this.sprites == null) {
			this.sprites = shadow.getAutomata().getController().getGame().sprites.getShadow();
		}
	}
	
	public AvatarShadow(Shadow shadow) {
		this.shadow = shadow;
		this.animMoveSpeed = 50;
	}
	
	/**
	 * Paint le sprite selectionné par m_imageIndex et découpe le 
	 * déplacement en indice selon le nombre de frames de l'animation
	 */
	@Override
	public void paint(Graphics g, int x, int y, int dx, int dy) {
		this.checkSprite();
		
		BufferedImage printSprite = sprites[m_imageIndex];

		int pas = dx / NB_IMAGES;
		int temp_x = x;
		int temp_y = y;
		
		if (moving) { 
			switch (orientation) {
			case EAST:
				//EST
				temp_x = x  - ((NB_IMAGES-countFrame) * pas);
				break;
			case WEST:
				//WEST
				temp_x = x + ((NB_IMAGES-countFrame) * pas);
				break;
			case NORTH:
				//NORTH
				temp_y = y  + ((NB_IMAGES-countFrame) * pas);
				break;
			case SOUTH:
			//SOUTH
				temp_y = y - ((NB_IMAGES-countFrame) * pas);
				break;
			}
		}	
		
		g.drawImage(printSprite, temp_x, temp_y, dx, dy, null);
	}
}
