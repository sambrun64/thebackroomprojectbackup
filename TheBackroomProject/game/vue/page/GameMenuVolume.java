package vue.page;

import java.awt.Graphics;

import vue.Bar;
import vue.Button;
import vue.ViewController;
import vue.menu.MenuGame;

public class GameMenuVolume implements IPage {
	private final int BOUTONS = 3;
	private Button[] bouton = new Button[BOUTONS];
	private Bar volume_bar;
	private final int BACK_MENU = 0;
	private final int MUTE_MUSIC = 1;
	private final int CHANGE_MUSIC = 2;
	private int wScreen, hScreen;
	private ViewController game;
	private MenuGame inGame;

	/**
	 * Constructeur de la page consacrée à la gestion du volume de la musique
	 * @param game
	 * @param inGame
	 */
	public GameMenuVolume(ViewController game, MenuGame inGame) {
		this.wScreen = ViewController.WINDOW_WIDTH;
		this.hScreen = ViewController.WINDOW_HEIGHT;
		this.bouton[0] = new Button(this.wScreen / 2, this.hScreen / 2 - 100, "Back to menu");
		this.bouton[1] = new Button(this.wScreen / 2, this.hScreen / 2, "Mute");
		this.volume_bar = new Bar(this.wScreen / 2, this.hScreen / 2 + 100,
				"Volume : ", game.getVolume());
		this.bouton[2] = new Button(this.wScreen / 2, this.hScreen / 2 + 200, "Change music...");
		this.game = game;
		this.inGame = inGame;
	}

	public void buttonClicked(int x, int y) {
		for (int i = 0; i < BOUTONS; i++) {
			if (this.bouton[i].checkClickOn(x, y)) {
				break;
			}
		}
		this.game.changeVolume(this.volume_bar.clickOn(x, y));
	}

	public void buttonClickReleased(int x, int y) {
		int action = -1;
		for (int i = 0; i < BOUTONS; i++) {
			if (this.bouton[i].checkClickToRelease(x, y)) {
				action = i;
				break;
			}
		}
		switch (action) {
		case BACK_MENU:
			this.inGame.changePage("Main");
			break;
		case MUTE_MUSIC:
			this.game.sOnOffMusic();
			break;
		case CHANGE_MUSIC:
			this.inGame.changePage("Musics");
		default:
			break;
		}
	}

	public void paint(Graphics g, int l, int h) {
		this.wScreen = l;
		this.hScreen = h;
		this.bouton[0].paint(g, this.wScreen / 2, this.hScreen / 2 - 100);
		this.bouton[1].paint(g, this.wScreen / 2, this.hScreen / 2      );
		this.volume_bar.paint(g,this.wScreen / 2, this.hScreen / 2 + 100);
		this.bouton[2].paint(g, this.wScreen / 2, this.hScreen / 2 + 200);
	}
}
