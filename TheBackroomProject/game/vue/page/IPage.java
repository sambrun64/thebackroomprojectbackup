package vue.page;

import java.awt.Graphics;

/**
 * Interface des pages des menus.
 * Chaque page comporte un nombre différent de boutons, de sélecteurs et de jauges, 
 * mais chaque page doit intéragir de la même façon avec les pages qui l'intégrent.
 */
public interface IPage {
	/**
	 * Envoie les coordonnées du clic à la page
	 * @param x
	 * @param y
	 */
	public void buttonClicked(int x, int y);
	
	/**
	 * Envoie les coordonées du relachement du clic à la page
	 * @param x
	 * @param y
	 */
	public void buttonClickReleased(int x, int y);
	
	/**
	 * Donne l'ordre d'afficher la page à l'écran
	 * @param g
	 * @param l
	 * @param h
	 */
	public void paint(Graphics g, int l, int h);
}
