package vue.page;

import java.awt.Graphics;

import vue.Button;
import vue.DoubleSelector;
import vue.ViewController;
import vue.menu.MenuGame;

public class GameMenuAutomataMain implements IPage {

	private DoubleSelector selector;
	private Button button;
	private int wScreen, hScreen;
	private ViewController game;
	private MenuGame inGame;

	/**
	 * Constructeur de la page qui gère l'automate de chaque entité.
	 * Permet le transfert.
	 * @param game
	 * @param inGame
	 */
	public GameMenuAutomataMain(ViewController game, MenuGame inGame) {
		this.game = game;
		this.inGame = inGame;
		this.wScreen = ViewController.WINDOW_WIDTH;
		this.hScreen = ViewController.WINDOW_HEIGHT;
		this.button = new Button(this.wScreen / 2, 120, "Back to menu");
		String[] items = this.game.getAutomatonNames();
		String[] entries = this.game.getEntityNames();
		int[] choices = new int[entries.length];
		for (int i = 0 ; i < entries.length ; i++) {
			choices[i] = this.game.getAutomateIDof(i);
		}
		this.selector = new DoubleSelector(this.wScreen / 2, this.hScreen / 2,
				entries, items, choices);
	}

	@Override
	public void buttonClicked(int x, int y) {
		this.selector.clickOn(x, y);
		this.button.checkClickOn(x, y);
	}

	@Override
	public void buttonClickReleased(int x, int y) {
		// The double selector choice
		int action = this.selector.checkClickToRelease(x, y);
		if (action != DoubleSelector.NOTHING_TO_DO) {
			this.game.setAutomaton(this.selector.getSelectedEntryID(), action);
		}
		// The "Back to main menu" button action
		if (this.button.checkClickToRelease(x, y)) {
			this.inGame.changePage("Main");
		}
	}

	@Override
	public void paint(Graphics g, int l, int h) {
		this.wScreen = l;
		this.hScreen = h;
		this.selector.paint(g, l/2, h/2);
		this.button.paint(g, l/2, 120);
	}
}
