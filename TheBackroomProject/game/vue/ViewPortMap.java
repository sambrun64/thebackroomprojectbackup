package vue;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;

import model.Case;
import model.Map;
import model.entity.Player;

/**
 * ViewPort : Gere l'affichage en mode viewport du jeu
 */
public class ViewPortMap {
	private ArrayList<Case> liste;
	private int mouseX,mouseY;
	private Case selectCase;
	private int selCaseX,selCaseY;
	
	private int viewPortSizeRayLine, viewPortSizeRayCol;
	private Player focusPlayer;
	private Map m;
	private ViewPortController view;
	
	public ViewPortMap(ViewPortController view,Map m) {
		this.viewPortSizeRayLine = view.getViewPortSizeRayLine();
		this.viewPortSizeRayCol = view.getViewPortSizeRayCol();
		this.focusPlayer = view.getFocusPlayer();
		this.m = m;
		this.view = view;
	}
	
	public void mouseMove(int x,int y) {
		this.mouseX = x;
		this.mouseY = y;
	}
	
	public void clickReleaseAs(int x, int y,int button) {
		view.shot(this.mouseX, this.mouseY, button, selectCase,viewPortSizeRayLine,viewPortSizeRayCol);
	}

	/**
	 * Produit l'affichage du jeu
	 * 
	 * @param g Graphics a peindre
	 * @param l largeur de l'espace d'affichage
	 * @param h hauteur de l'espace d'affichage
	 */
	public void paint(Graphics g, int l, int h,float k,float m) {
		ArrayList<Case> listeCaseEntity=new ArrayList<>();
		Case c;
		// récupération des éléments du viewport
		this.liste = this.m.getViewPortCoord(this.viewPortSizeRayLine, this.viewPortSizeRayCol, this.focusPlayer);

		int lineVP = this.m.getViewPortSizeLine();
		int colVP = this.m.getViewPortSizeCol();

		int lCase = l / colVP;
		int hCase = h / lineVP;
		// affichage de chaque élément du vp
		Iterator<Case> it = liste.iterator();
		int i = 0, j = 0;
		while (it.hasNext()) {
			
			c = it.next();
			
			this.view.paintCase(g, c, (int)((j-1+k)* lCase), (int)((i-1+m)*hCase)	, lCase, hCase);
			if(this.mouseX >= (int)((j-1+k)* lCase) && this.mouseX <(int)((j+k)* lCase) && this.mouseY >= (int)((i-1+m)*hCase) && this.mouseY < (int)((i+m)*hCase)) {
				//view.brightCase(g, c,(int)((j-1+k)*lCase), (int)((i-1+m)*hCase), lCase, hCase);
				selectCase = c;
				selCaseX = (int)((j-1+k)*lCase);
				selCaseY = (int)((i-1+m)*hCase);
			}
			
			if(c.hasGameItem()) {
				listeCaseEntity.add(c);
			}
			// calcul des coordonnées
			j++;
			if (j >= colVP+2) {
				j = 0;
				i++;
			}
		}
		//affichage des entités
		view.paintEntities(g, lCase, hCase,k,m);
		view.paintBullet(g, lCase, hCase,k,m);


		view.brightCase(g, selectCase,selCaseX,selCaseY, lCase, hCase,l,h);
		
		view.paintMentalHealth(g,l,h);
		
		view.paintInterface(g, focusPlayer);

	}
	
	public ArrayList<Case> getListe(){
		return liste;
	}
}
