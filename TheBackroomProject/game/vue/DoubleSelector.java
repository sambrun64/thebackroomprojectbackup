package vue;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import utils.ImgUtils;

public class DoubleSelector {
	// Constante indiquant qu'il n'y a rien à faire
	public static final int NOTHING_TO_DO = -1;
	
	private BufferedImage back, titlecase;
	private BufferedImage buttonDown, buttonDownR, buttonUp, buttonUpR;
	private BufferedImage buttonLeft, buttonLeftR, buttonRight, buttonRightR;
	private boolean upClicked = false, downClicked = false, leftClicked = false, rightClicked = false;
	private String[] items;
	private String[] entries;
	private int[] choices; // Sized as tall as this.entries,
	// each int of this.choices is an ID valued between 0 and this.items.length-1
	private int first = 0, entry = 0;
	private int posX, posY, width, height, totalHeight;
	private Font titleFont;
	private boolean[] selection = new boolean[3];
	
	/**
	 * Constructeur du sélecteur double.
	 * Un sélecteur double joue le rôle d'un tableau associatif à double entrée qui affecte à
	 * chaque entrée (de la liste entries) un élément parmi la liste items.
	 * Le paramètre tableau choices indique les données initiales de chaque entrée lors de la 
	 * construction du sélecteur double.
	 * 
	 * Les coordonnées passées en paramètre (x,y) indiquent la position du centre du 
	 * sélecteur double.
	 * 
	 * Deux boutons fléchés droite et gauche permettent de switcher entre chaque entrée.
	 * Deux boutons fléchés haut et bas permettent de parcourir l'ensemble des items qu'on peut.
	 * affecter à chaque entrée.
	 * Le sélecteur double ne permet d'associer qu'un seul élément à chaque entrée.
	 * Pour chaque entrée, l'élément sélectionné est colorié en jaune à l'affichage. Les autres en blanc
	 * 
	 * @param x
	 * @param y
	 * @param entries
	 * @param items
	 * @param choices
	 */
	public DoubleSelector(int x, int y, String[] entries, String[] items, int choices[]) {
		this.items = new String[items.length];
		this.entries = new String[entries.length];
		this.choices = new int[choices.length];
		System.arraycopy(items  , 0, this.items,   0, items.length);
		System.arraycopy(entries, 0, this.entries, 0, entries.length);
		System.arraycopy(choices, 0, this.choices, 0, choices.length);
		this.init(x, y);
	}
	
	/**
	 * Initialise les données géométriques et les images du sélecteur double en fonction des
	 * coordonnées (x,y) de son centre.
	 * @param x
	 * @param y
	 */
	private void init(int x, int y) {
		this.titleFont  = new Font("resources/consola.ttf", Font.PLAIN, 24);
		this.titlecase  = ImgUtils.loadImg("resources/button.png");
		this.back       = ImgUtils.loadImg("resources/bar.png");
		this.buttonDown = ImgUtils.loadImg("resources/button/down.png");
		this.buttonDownR= ImgUtils.loadImg("resources/button/down_r.png");
		this.buttonUp   = ImgUtils.loadImg("resources/button/up.png");
		this.buttonUpR  = ImgUtils.loadImg("resources/button/up_r.png");
		this.buttonLeft = ImgUtils.loadImg("resources/button/left.png");
		this.buttonLeftR= ImgUtils.loadImg("resources/button/left_r.png");
		this.buttonRight= ImgUtils.loadImg("resources/button/right.png");
		this.buttonRightR=ImgUtils.loadImg("resources/button/right_r.png");
		this.width = this.back.getWidth();
		this.height= this.back.getHeight();
		this.totalHeight = this.height * 4;
		this.posX = x - this.width / 2;
		this.posY = y - this.totalHeight / 2;
		this.selection[0] = this.selection[1] = this.selection[2] = false;
	}
	
	/**
	 * Rafraîchit les données géométriques du sélecteur double en fonction des nouvelles
	 * coordonées du centre
	 * 
	 * @param x
	 * @param y
	 */
	private void refreshCoords(int x, int y) {
		this.width = this.back.getWidth();
		this.height= this.back.getHeight();
		this.totalHeight = this.height * 4;
		this.posX = x - this.width / 2;
		this.posY = y - this.totalHeight / 2;
	}
	
	/**
	 * Retourne le tableau des choix
	 * @return
	 */
	public int[] getChoices() {
		return this.choices;
	}
	
	/**
	 * Retourne le nom de l'élément sélectionné pour l'entrée actuellement affichée
	 * @return
	 */
	public String getSelectedItem() {
		return this.items[this.choices[this.entry]];
	}
	
	/**
	 * Retourne le nom de l'entrée actuellement affichée
	 * @return
	 */
	public String getSelectedEntry() {
		return this.entries[this.entry];
	}
	
	/**
	 * Retourne l'ID de l'élément sélectionné pour l'entrée actuellement affichée
	 * @return
	 */
	public int getSelectedItemID() {
		return this.choices[this.entry];
	}
	
	/**
	 * Retourne l'ID de l'entrée actuellement affichée
	 * @return
	 */
	public int getSelectedEntryID() {
		return this.entry;
	}
	
	/**
	 * Retourne le nombre d'éléments sélectionnables
	 * @return
	 */
	public int size() {
		return this.items.length;
	}
	
	/**
	 * Réagit à un clic
	 * Doit être appelé à chaque événement de clic !
	 * @param x
	 * @param y
	 */
	public void clickOn(int x, int y) {
		// Check if clicked on up button
		this.upClicked = (x >= this.posX && x <= this.posX + this.height &&
    			y >= this.posY && y <= this.posY + this.height) ? true : false;
		// Check if clicked on down button
		this.downClicked = (x <= this.posX + this.width && x >= this.posX + this.width - this.height &&
				y >= this.posY && y <= this.posY + this.height) ? true : false;
		// Check if clicked on left button
		this.leftClicked = (x <= this.posX + 2*this.height && x >= this.posX + this.height &&
				y >= this.posY && y <= this.posY + this.height) ? true : false;
		// Check if clicked on right button
		this.rightClicked = (x <= this.posX + this.width - this.height &&
				x >= this.posX + this.width - 2 * this.height &&
				y >= this.posY && y <= this.posY + this.height) ? true : false;
		// Check if clicked on one choice
		for (int i = 1 ; i < 4 ; i++) {
			if (x >= this.posX && x <= this.posX + this.width &&
					y >= this.posY + i*this.height && y <= this.posY + (i+1)*this.height) {
				this.selection[i-1] = true;
				break;
			}
		}
    }
	/**
	 * Réagit au relâchement d'un clic.
	 * Doit être appelé à chaque événement de relâchement d'un clic !
	 * 
	 * Retourne DoubleSelector.NOTHING_TO_DO (-1) si aucun changement n'a été effectué.
	 * Retourne l'ID de l'élement sélectionné de l'entrée affichée si cette dernière a changé
	 * d'élément sélectionné (un chiffre compris entre 0 et .size())
	 * @param x
	 * @param y
	 * @return
	 */
	public int checkClickToRelease(int x, int y) {
		// Check if clicked on up button
		if (x >= this.posX && x <= this.posX + this.height && this.upClicked &&
    			y >= this.posY && y <= this.posY + this.height) {
			this.first = (this.first > 0) ? this.first-1 : this.first;
			return NOTHING_TO_DO;
		}
		// Check if clicked on down button
		if (x <= this.posX + this.width && x >= this.posX + this.width - this.height &&
				y >= this.posY && y <= this.posY + this.height && this.downClicked) {
			this.first = (this.first < this.items.length - 2) ? this.first+1 : this.first;
			return NOTHING_TO_DO;
		}
		// Check if clicked on right button
		if (x <= this.posX + this.width - this.height && this.rightClicked &&
				x >= this.posX + this.width - 2 * this.height &&
				y >= this.posY && y <= this.posY + this.height) {
			if (this.entry < this.choices.length - 1) {
				this.entry++;
			}
			return NOTHING_TO_DO;
		}
		// Check if clicked on left button
		if (x <= this.posX + 2*this.height && x >= this.posX + this.height &&
				y >= this.posY && y <= this.posY + this.height && this.leftClicked) {
			this.entry = (this.entry > 0) ? this.entry-1 : this.entry;
			return NOTHING_TO_DO;
		}
		// Check if clicked on one choice
		for (int i = 1 ; i < 4 ; i++) {
			if (x >= this.posX && x <= this.posX + this.width && this.selection[i-1] &&
					y >= this.posY + i*this.height && y <= this.posY + (i+1)*this.height) {
				this.choices[this.entry] = (this.first + i - 1 < this.items.length) ? 
						this.first + i - 1 : this.choices[this.entry];
				this.selection[i-1] = false;
				return this.choices[this.entry];
			}
		}
		return NOTHING_TO_DO;
	}
	
	/**
	 * Envoie l'ordre de dessiner le sélecteur double sur le graphique g donné en paramètre
	 * en fonction des coordonnées (s,t)
	 * @param g
	 * @param s
	 * @param t
	 */
    public void paint(Graphics g, int s, int t) {
    	this.refreshCoords(s, t);
    	// Get window metrics and calculate absolute y of the first text
    	FontMetrics metrics = g.getFontMetrics();
    	int y = this.posY + this.height/ 2 + (metrics.getHeight() / 4);
    	g.setColor(Color.WHITE);
    	g.setFont(this.titleFont);
    	// Draw titlecase
    	g.drawImage(this.titlecase, 
    			this.posX, this.posY, 
    			this.width, this.height, null);
    	// Write the title
    	int x = this.posX + this.width / 2 - 
    			(metrics.stringWidth(this.entries[this.entry]));
    	g.drawString(this.entries[this.entry], x, y);
    	// Draw button for up
    	g.drawImage((this.upClicked) ? this.buttonUpR : this.buttonUp, 
    			this.posX, this.posY,
    			this.height, this.height, null);
    	// Draw button for down
    	g.drawImage((this.downClicked) ? this.buttonDownR : this.buttonDown, 
    			this.posX + this.width - this.height, this.posY,
    			this.height, this.height, null);
    	// Draw button for left
    	g.drawImage((this.leftClicked) ? this.buttonLeftR : this.buttonLeft, 
    			this.posX + this.height, this.posY,
    			this.height, this.height, null);
    	// Draw button for right
    	g.drawImage((this.rightClicked) ? this.buttonRightR : this.buttonRight, 
    			this.posX + this.width - 2 * this.height, this.posY,
    			this.height, this.height, null);
    	// Draw the three bars
    	for (int i = 1 ; i < 4 ; i++) {
        	g.drawImage(this.back,
        			this.posX, this.posY + i * this.height,
        			this.width, this.height, null);
    	}
    	// Write the three choices beginning at this.first
    	for (int i = this.first ; i < this.first + 3 ; i++) {
			g.setColor((i == this.choices[this.entry]) ? Color.YELLOW : Color.WHITE);
    		if (i < this.items.length) {
    			x = this.posX + this.width / 2 -
    					(metrics.stringWidth(this.items[i]));
    			g.drawString(this.items[i], x, y + (i+1-this.first) * this.height);
    		}
    	}
    }
}
