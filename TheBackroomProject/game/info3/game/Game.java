/*
 * Copyright (C) 2020  Pr. Olivier Gruber
 * Educational software for a basic game development
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  Created on: March, 2020
 *      Author: Pr. Olivier Gruber
 */
package info3.game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import automataEngine.AutomatonExe;

import controller.MainController;

import model.Case;
import model.Map;
import model.Spawner;
import model.entity.Entity;
import model.entity.Player;
import model.loot.Loot;
import model.weapon.Projectile;
import model.weapon.Weapon;
import utils.MathUtils;
import vue.Sprites;
import vue.ViewController;

/**
 * Game : Coeur du jeu
 */
public class Game {

	static Game game;

	public static void main(String args[]) throws Exception {
		try {
			System.out.println("Game starting...");
			utils.SpriteBank.load();
			game = new Game();
			System.out.println("Game started.");
		} catch (Throwable th) {
			th.printStackTrace(System.err);
		}
	}

	// NOS OBJECTS
	private static final int WIDTH = 60;
	private static final int HEIGH = 60;
	private static final int SPEED_PLAYER_IN_TICK = 50;
	private static final int SPEED_BULLET_IN_TICK = 25;
	private int tickCounter;
	private int tickCounterBullet=1;
	private int tickCounterSatiety=1;
	
	// Les 3 difficultés du jeu
	public static int difficulty = 3;
	public static final int EASY = 3;
	public static final int HARD = 2;
	public static final int NUKE = 1;

	/* Map courante */
	private Map map;

	/* Les entités courante du jeu a la fin du tick les listes sont merges */
	private ArrayList<Entity> entities;
	private ArrayList<Entity> newEntities;
	private ArrayList<Entity> removeEntity;
	
	/* Liste des projectiles */
	private ArrayList<Entity> listProjectile;
	private ArrayList<Entity> removeBullet;
	/* arme courante */
	private Weapon weapon;
	/* Liste des loots du jeu */
	private ArrayList<Loot> Loots;
	
	/* joueur principale */
	private Player j1;

	/* vue du jeu */
	private ViewController viewControl;
	/* controle du jeu */
	private MainController control;
	/* spawner des monstres */
	private Spawner spawner;
	
	/* variables pour gerer les différents modes / automates du joueur */
	/* servent a ne pas changer l'automate pendant qu'il execute son step */
	public boolean doPossession = false;
	public boolean doShadow = false;

	/* graphiques */
	public Sprites sprites;
	
	/* nombre de niveaux parcourues */
	public int stageNumber = 0;
	
	/**
	 * ajout d'une entité a la liste des nouvelles entitées
	 * 
	 * @param e l'entité a ajouté
	 */
	public void addNewEntities(Entity e){
		this.newEntities.add(e);
		// Changer l'automate de base selon la difficulté (implémentation bonus)
		switch (Game.difficulty) {
		case Game.EASY:
			this.control.setAutomata(e);
			break;
		case Game.HARD:
			this.control.setAutomata(e);
			break;
		case Game.NUKE:
			if (e instanceof Player) {
				this.control.setAutomata(e);
			} else {
				/* on prend un automate au hasard */
				this.control.setAutomata(e,"EnemyBaseHit");
			}
			break;
		}
	}

	/**
	 * ajout d'une entité a la liste des nouvelles entitées
	 * 
	 * @param e l'entité a ajouté
	 */
	public void addRemoveEntities(Entity e){
		this.removeEntity.add(e);
	}
	
	/**
	 * ajout d'une entité a la liste des entitées a remove
	 * 
	 * @param e l'entité a ajouté
	 */
	public void addRemoveEntity(Entity e) {
		this.removeEntity.add(e);
	}
	
	/**
	 * ajout d'une balle a la liste des balles a remove
	 * 
	 * @param e l'entité balle a ajouté
	 */
	public void addRemoveBullet(Entity e) {
		this.removeBullet.add(e);
	}
	
	/**
	 * donne la liste des entitées
	 * 
	 * @return La liste des entitées 
	 */
	public ArrayList<Entity> getEntities(){
		return this.entities;
	}
	
	/**
	 * donne la liste des loots
	 * 
	 * @return La liste des loots 
	 */
	public ArrayList<Loot> getLoot(){
		return this.Loots;
	}

	/**
	 * Creation du jeu
	 */
	Game() throws Exception {
		// NOS INITIALISATIONS
		this.tickCounter = 0;
		// init du model
		initGame();

		// controlleur
		this.control = new MainController(this);

		// vues
		this.viewControl = new ViewController(this, this.control, this.map);
		this.viewControl.setMap(this.map);
		this.control.setView(this.viewControl);
		
		//entitées
		this.addNewEntities(j1);
		//spawner
		this.spawner = new Spawner(this, this.map);
		
		this.mergeEntities();
		
		//dernier truc a faire apres les init
		this.viewControl.startWindow();
	}

	/**
	 * init la partie model du jeu
	 */
	private void initGame() {
		/* création de la map */
		this.map = new Map(Game.WIDTH, Game.HEIGH, Game.difficulty);
		//this.map.printMap(); //affichage de la map
		
		/* c'est la map qui crée nos joueurs donc il faut le recuperer */
		this.j1 = this.map.getJ1();
		
		/* listes des objests */
		this.entities = new ArrayList<>();
		this.newEntities = new ArrayList<>();
		this.removeEntity = new ArrayList<>();
		this.Loots=new ArrayList<>();
		this.listProjectile=new ArrayList<>();
		this.removeBullet=new ArrayList<>();
		
		/* armes du jeu */
		this.weapon=new Weapon();
		
		
		/* chargement des sprites */
		try {
			this.sprites = new Sprites();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creer un  étage
	 */
	public void newStage() {
		// si il y avait une possession, on la suppr
		this.j1.destroyPossession();
		
		//on regenere la map et tout ce qui va avec
		initGame();
		this.viewControl.newStage(map);
		
		this.addNewEntities(j1);
		
		this.spawner = new Spawner(this, this.map);
		this.mergeEntities();
		
		this.j1.destroyShadow();
		this.stageNumber ++;
	}
	
	/**
	 * donne la liste de tous les noms de projectiles
	 * 
	 * @return La liste des projectiles
	 */
	public String[] getProjectileNames() {
		String[] retour = new String[this.listProjectile.toArray().length];
		for (int i = 0 ; i < retour.length ; i++) {
			retour[i] = this.listProjectile.get(i).toString();
		} return retour;
	}
	
	/**
	 * donne la liste de tous les noms d'entitées
	 * 
	 * @return La liste des noms entitées
	 */
	public String[] getEntityNames() {
		String[] retour_ = new String[this.entities.toArray().length];
		for (int i = 0 ; i < retour_.length ; i++) {
			retour_[i] = this.entities.get(i).toString();
		} return retour_;
	}
	
	/**
	 * @param index balle x
	 * @return L'id de la balle x
	 */
	public int getProjAutomateIDof(int index) {
		return this.control.getAutomata().indexOf(this.listProjectile.get(index).getAutomata());
	}
	
	/**
	 * @param index entitée e
	 * @return L'id de l'entitée e
	 */
	public int getAutomateIDof(int index) {
		return this.control.getAutomata().indexOf(this.entities.get(index).getAutomata());
	}

	/**
	 * @return id du joueur
	 */
	public int getIDPlayer1Automate() {
		return this.control.getAutomata().indexOf(this.j1.getAutomata());
	}
	
	/**
	 * donne la liste des noms d'automates
	 * 
	 * @return La liste des noms des différents automates 
	 */
	public String[] getAutomatonNames() {
		Iterator<AutomatonExe> it = this.control.getAutomata().iterator();
		String[] retour = new String[this.control.getAutomata().size()];
		int index = 0;
		while (it.hasNext()) {
			retour[index++] = it.next().getName();
		} return retour;
	}
	
	/**
	 * set un automate a un projectile
	 * 
	 * @param index_e de l'entitée 
	 * @param index_a de l'automate 
	 */
	public void setProjAutomaton(int index_e, int index_a) {
		AutomatonExe a = this.control.getAutomata().get(index_a);
		Entity e = this.listProjectile.get(index_e);
		this.control.setAutomata(e, a);
	}
	
	/**
	 * set un automate a une entitée
	 * 
	 * @param index_e de l'entitée 
	 * @param index_a de l'automate  
	 */
	public void setAutomaton(int index_entity, int index_auto) {
		Entity e = this.entities.get(index_entity);
		AutomatonExe a = this.control.getAutomata().get(index_auto);
		this.control.setAutomata(e, a);
	}

	/**
	 * set l'automate au joueur 1
	 * 
	 * @param index_a de l'automate 
	 */
	public void setPlayer1Automaton(int index) {
		this.control.setAutomata(this.j1, this.control.getAutomata().get(index));
	}
	
	/**
	 * tire un coups de feu
	 * 
	 * @param e l'entitée qui tire
	 * @param mouseX pos en x du shoot
	 * @param mouseY pos en y du shoot
	 * @param button cliqué
	 * @param c case actuel
	 * @param nbLine taille en ligne 
	 * @param nbCol taille en colonnes 
	 */
	public void shot(Entity e,int mouseX,int mouseY, int button, Case c,int nbLine,int nbCol) {
		/*si clique droit on change de mode sinon on tire*/
		if(button==1) {
			int nbAmmo = this.j1.getCurrentAmmo();
			int min;
			if(this.weapon.getMode() == Weapon.MODE_MAGNUM) {
				min = 0;
			} else {
				min = 2;
			}
			if(nbAmmo > min  && this.j1.getState() == Player.fusion) {
				this.j1.setCurrentAmmo(nbAmmo-(min+1));
				ArrayList<Projectile> tempList=this.weapon.shot(e,mouseX, mouseY,viewControl.getWidth(),viewControl.getHeight(),this.map,nbLine,nbCol);
				for(Projectile bullet: tempList) {
					this.control.setAutomata(bullet);
					this.listProjectile.add(bullet);
				}
			}
		}else {
			this.weapon.changeMode();
		}
	}
	
	/**
	 * demande au j1 de faire la possession
	 */
	public void possession() {
		if(doPossession) {
			j1.createPossession();
			this.control.setAutomata(j1.getPossessEntity(),"OmbrePossess");
			doPossession = false;
			
		}
	}
	
	/**
	 * demande au j1 de faire le split
	 */
	public void shadow() {
		if(doShadow) {
			j1.switchAutomata();
			doShadow = false;
		}
	}

	/*
	 * This method is invoked almost periodically, given the number of milli-seconds
	 * that elapsed since the last time this method was invoked.
	 */
	public void tick(long elapsed) {
		/* verif mort du joueur */
		if (this.j1.isDead()) {
			this.viewControl.gameOver();
			this.stageNumber = 0;
		}

		/* verif timming de jeu */
		if (this.tickCounter > Game.SPEED_PLAYER_IN_TICK) {
			this.tickCounter = 0;
			
			//calcul du chemin vers j1
			j1.getMap().setMapChemin();
			
			//on fait jouer tt les entitées
			for(Entity e : this.entities){
				if(!this.removeEntity.contains(e)) {
					e.getAutomata().step(e);	
					//il se peut qu'on ai une possession/ombre
					this.possession();
					this.shadow();
				}
			}
			
			//verif de l'arrivée sur la map
			if (map.getIsOnExit()) {
				newStage();
				System.out.println("Nouvelle map");
			}
			
			// on fait repop monstre / objets
			this.spawner.updateSpawn();
			// merge des entitées a suppr / a creer
			mergeEntities();
			
		}
		
		/* verif timing des balles */
		if (this.tickCounterBullet > Game.SPEED_BULLET_IN_TICK) {
			this.tickCounterBullet=0;
			this.weapon.tick();
			for(Entity bullet:listProjectile) {
					bullet.getAutomata().step(bullet);
			}
			for(Entity bulletDeleted:this.removeBullet) {
				listProjectile.remove(bulletDeleted);
			}
		}
		/* verif timing des jauges */
		if (MathUtils.realModulo(this.tickCounterSatiety,500) == 0) {
			if(this.j1.getCurrentSatiety()>0) {
				this.j1.setCurrentSatiety(this.j1.getCurrentSatiety()-1);
			}
			else {
				this.j1.setCurrentHealth(this.j1.getCurrentHealth()-1);
			}
		}
		this.tickCounterSatiety++;
		this.tickCounterBullet++;
		this.tickCounter++;

	}
	
	/**
	 * fusionnes les listes des entitées
	 */
	private void mergeEntities() {
		for(Entity e : this.removeEntity) {

			this.map.getCaseAt(e.getPosLine(), e.getPosCol()).removeGameItem();

			this.entities.remove(e);
			if(e == j1.getPossessEntity()) {
				j1.destroyInstantPossession();
			}
		}
		
		for(Entity e : this.newEntities) {
			this.entities.add(e);
		}
		
		this.newEntities.clear();
		this.removeEntity.clear();
	}
	
	/**
	 * donne la liste des projectiles 
	 * 
	 * @return liste des projectiles
	 */	
	public ArrayList<Entity> getProjectile(){
		return this.listProjectile;
	}
	
	/**
	 * donne l'arme actuelle
	 * 
	 * @return arme
	 */
	public  Weapon getWeapon() {
		return this.weapon;
	}

	/**
	 * set la difficultée du jeu
	 * 
	 * @param difficulty_choice difficultée souhaité
	 */
	public void changeGameDifficulty(int difficulty_choice) {
		Game.difficulty = 3 - difficulty_choice;
	}

}