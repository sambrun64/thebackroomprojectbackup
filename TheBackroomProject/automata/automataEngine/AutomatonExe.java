package automataEngine;

import java.util.ArrayList;

import controller.MainController;
import model.entity.Entity;

public class AutomatonExe {
	private String name;
	private StateExe initialState;
	private ArrayList<StateExe> listState= new ArrayList<>();
	
	private MainController mc;
	
	/**
	 * 
	 * @param name
	 * set le name
	 */
	public AutomatonExe(String name){
		this.name = name;
	}
	
	/**
	 * 
	 * @return le name
	 * get name
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * 
	 * @return l'état initial
	 * get l'état initial
	 */
	public StateExe getinitialState(){
		return this.initialState;
	}
	
	/**
	 * @param MainController
	 * @return l'état initial
	 * set à l'automate le MainController
	 */
	public void setController(MainController mc) {
		this.mc = mc;
	}
	
	/**
	 * @param
	 * @return MainControllet
	 * get le MainController
	 */
	public MainController getController() {
		return this.mc;
	}
	
	/**
	 * @param StateExe newState
	 * ajoute à l'automate un nouvel état
	 */
	public void add(StateExe newState){
		listState.add(newState);
	}
	
	/**
	 * @return ArrayList<StateExe> 
	 * get la liste des états
	 */
	public ArrayList<StateExe>  getstates(){
		return this.listState;
	}
	
	/**
	 * @param StateExe state
	 * set un état initial à l'automate
	 */
	public void initState(StateExe state) {
		this.initialState=state;
	}
	
	/**
	 * @param Entity e
	 * execute un pas de l'automate
	 */
	public void step(Entity e) {
		e.getCurrentState().step(e);
	}
}
