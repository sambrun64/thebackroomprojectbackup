package condition;

import java.util.ArrayList;
import java.util.List;

import model.Case;
import model.TypeCase;
import model.entity.Entity;
import model.entity.Player;
import utils.MathUtils;

public class CondClossest implements ICondition{
	private List<String> param;
	
	public CondClossest(List<String> parametre) {
		this.param = parametre;
	}

	@Override
	public boolean eval(Entity e) {
		
		
		if(this.param == null || this.param.size() <= 1) {
			this.param.add(0,"@");
			this.param.add(1,"F");
		}
		
		// IL VA FALOIR GERE LES MODULOS
		
		String dir = this.param.get(1);
		//utilise le grandient
		return dir.equals(MathUtils.wayEntity(e,e.getMap().getMapChemin()));
		/*switch(dir) {
		case "N":
		case "F":
			for(int i = e.getPosCol() - 1; i <= e.getPosCol() +1;i++) {
				for(int j = e.getPosLine()-1; j > 0;j--){
					listeCase.add(e.getMap().getCaseAt(j, i));
				}
			}
			break;
		case "E":
		case "R":
			for(int i = e.getPosLine() - 1; i <= e.getPosLine() +1;i++) {
				for(int j = e.getPosCol()+1; j < e.getMap().getNbCol();j++){
					listeCase.add(e.getMap().getCaseAt(i, j));
				}
			}
			break;
		case "W":
		case "L":
			for(int i = e.getPosLine() - 1; i <= e.getPosLine() +1;i++) {
				for(int j = e.getPosCol()-1; j > 0;j--){
					listeCase.add(e.getMap().getCaseAt(i, j));
				}
			}
			break;
		case "S":
		case "B":
			for(int i = e.getPosCol() - 1; i <= e.getPosCol() +1;i++) {
				for(int j = e.getPosLine()+1; j < e.getMap().getNbLine();j++){
					listeCase.add(e.getMap().getCaseAt(j, i));
				}
			}
			break;
		default:
			for(int i = e.getPosCol() - 1; i <= e.getPosCol() +1;i++) {
				for(int j = e.getPosLine()-1; j > 0;j--){
					listeCase.add(e.getMap().getCaseAt(j, i));
				}
			}
			break;
		}
		
		
		String cat = this.param.get(0);
		
		switch(cat) {
		
		case "@":
		case "T":
			for(Case c : listeCase) {
				if(c.getGameItem() instanceof Player) {
					return true;
				}
			}
			return false;
		case "A":
//			for(Case c : listeCase) {
//				if(c.getGameItem() instanceof Ennemy) {
//					return true;
//				}
//			}
//			return false;
		case "O":
			for(Case c : listeCase) {
				if(c.getTypeCase() == TypeCase.WALL) {
					return true;
				}
			}
			return false;
		case "V":
			for(Case c : listeCase) {
				if(c.getTypeCase() == TypeCase.GROUND) {
					return true;
				}
			}
			return false;
		default:
			return false;
		}*/
		
	}

}
