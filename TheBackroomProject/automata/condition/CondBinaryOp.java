package condition;

import model.entity.Entity;

public class CondBinaryOp implements ICondition{
	private String operator;
	private ICondition left;
	private ICondition right;
	
	public CondBinaryOp(String op,ICondition left,ICondition right) {
		this.operator=op;
		this.left=left;
		this.right=right;
	}
	
	/**
	 * @param Entity e
	 * @return boolean
	 * calcul les deux conditions left et right 
	 * et en fonction de l'opérateur renvoie le bon boolean
	 */
	@Override
	public boolean eval(Entity e) {
		switch(this.operator) {
		case "&":
			return this.left.eval(e)&&this.right.eval(e);
		case "/":
			return this.left.eval(e)||this.right.eval(e);
		}
		return false;
	}
	

}
