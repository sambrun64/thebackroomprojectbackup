package condition;

import java.util.ArrayList;
import java.util.List;

import model.entity.Entity;

public class CondKey implements ICondition{

	private List<String> param;
	
	public CondKey(List<String> parametre) {
		this.param=parametre;
	}
	
	/**
	 * @param Entity e
	 * @return boolean
	 * vérifie si la touche tapé est celle du paramètre
	 */
	@Override
	public boolean eval(Entity e) {
		
		// accède à la liste des inputs
		ArrayList<Integer> inputs = e.getAutomata().getController().getInputs();
		
		if(this.param.size() == 0 || this.param == null) {
			this.param.add("Z");
		}
		
		String str = this.param.get(0).toUpperCase();
		
		if(inputs == null) {
			return false;
		}
		// vétrifie si l'input est le paramètre
		return inputs.contains((int)str.charAt(0));
		
	}

}
