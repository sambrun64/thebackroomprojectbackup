package action;

import java.util.List;

import model.entity.Entity;

public class ActExplode implements IAction{
	private int percent;
	private List<String> param;
	
	public ActExplode(List<String> parametre,int percent) {
		this.param=parametre;
		this.percent=percent;
	}

	public int getpercent() {
		return this.percent;
	}
	/**
	 * @param Entity e
	 * @return boolean
	 * Apelle la fonction explode dans l'Entity e
	 * détruit l'entité
	 */
	@Override
	public boolean apply(Entity e) {
		e.explode();
		return false;
	}

}
