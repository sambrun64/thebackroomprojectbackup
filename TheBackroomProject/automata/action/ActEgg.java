package action;

import java.util.List;

import model.Case;
import model.TypeCase;
import model.entity.Entity;
import model.entity.Hound;
import model.entity.Player;
import model.entity.Slime;
import model.entity.TypeEntity;
import model.entity.Zombie;
import utils.MathUtils;

public class ActEgg implements IAction{
	private int percent;
	private List<String> param;
	
	public ActEgg(List<String> parametre,int percent) {
		this.param=parametre;
		this.percent=percent;
	}
	
	public int getpercent() {
		return this.percent;
	}
	
	/**
	 * @param Entity e
	 * @param boolean
	 * Creer une nouvelle entity de lui même dans une direction en paramètre 
	 */
	@Override
	public boolean apply(Entity e) {
		
		Case c = null;
		
		int tailleMaxCol = e.getMap().getNbCol();
		int tailleMaxLine = e.getMap().getNbLine();
		
		if(this.param.size() == 0 || this.param == null) {
			/*
			 * si il n'y a aucun paramètre
			 * Choisit une direction en fonction d'un orde de priorité
			 * et vérifie si la case est libre
			 */
			
			Case temp;
			
			temp = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()+1, tailleMaxLine), MathUtils.realModulo(e.getPosCol()-1, tailleMaxCol));
			
			if((temp.getTypeCase() == TypeCase.NORMALGROUND || temp.getTypeCase() == TypeCase.WATERGROUND || temp.getTypeCase() == TypeCase.FIREGROUND) && !temp.hasGameItem()) {
				c = temp;
			}
			
			temp = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()+1, tailleMaxLine),MathUtils.realModulo(e.getPosCol()+1, tailleMaxCol));
			
			if((temp.getTypeCase() == TypeCase.NORMALGROUND || temp.getTypeCase() == TypeCase.WATERGROUND || temp.getTypeCase() == TypeCase.FIREGROUND) && !temp.hasGameItem()) {
				c = temp;
			}
			
			temp = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()-1, tailleMaxLine), MathUtils.realModulo(e.getPosCol()-1, tailleMaxCol));

			if((temp.getTypeCase() == TypeCase.NORMALGROUND || temp.getTypeCase() == TypeCase.WATERGROUND || temp.getTypeCase() == TypeCase.FIREGROUND) && !temp.hasGameItem()) {
				c = temp;
			}
			
			temp = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()-1, tailleMaxLine), MathUtils.realModulo(e.getPosCol()+1, tailleMaxCol));

			if((temp.getTypeCase() == TypeCase.NORMALGROUND || temp.getTypeCase() == TypeCase.WATERGROUND || temp.getTypeCase() == TypeCase.FIREGROUND) && !temp.hasGameItem()) {
				c = temp;
			}
			
			temp = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()+1, tailleMaxLine), e.getPosCol());

			if((temp.getTypeCase() == TypeCase.NORMALGROUND || temp.getTypeCase() == TypeCase.WATERGROUND || temp.getTypeCase() == TypeCase.FIREGROUND) && !temp.hasGameItem()) {
				c = temp;
			}
			
			temp = e.getMap().getCaseAt(e.getPosLine(),MathUtils.realModulo(e.getPosCol()+1, tailleMaxCol));

			if((temp.getTypeCase() == TypeCase.NORMALGROUND || temp.getTypeCase() == TypeCase.WATERGROUND || temp.getTypeCase() == TypeCase.FIREGROUND) && !temp.hasGameItem()) {
				c = temp;
			}
			
			temp = e.getMap().getCaseAt(e.getPosLine(),MathUtils.realModulo(e.getPosCol()-1, tailleMaxCol));
			
			if((temp.getTypeCase() == TypeCase.NORMALGROUND || temp.getTypeCase() == TypeCase.WATERGROUND || temp.getTypeCase() == TypeCase.FIREGROUND) && !temp.hasGameItem()) {
				c = temp;
			}
			
			temp = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()-1, tailleMaxLine), e.getPosCol());
			
			if((temp.getTypeCase() == TypeCase.NORMALGROUND || temp.getTypeCase() == TypeCase.WATERGROUND || temp.getTypeCase() == TypeCase.FIREGROUND) && !temp.hasGameItem()) {
				c = temp;
			}
			
		}
		
		if(this.param == null) {
			/*
			 * Attribue le paramète sélectionné
			 */
		
			String str = this.param.get(0);
			
			switch(str) {
			
			case "N":
			case "F":
				c = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()-1, tailleMaxLine), e.getPosCol());
				break;
			case "W":
			case "L":
				c = e.getMap().getCaseAt(e.getPosLine(),MathUtils.realModulo(e.getPosCol()-1, tailleMaxCol));
				break;
			case "E":
			case "R":
				c = e.getMap().getCaseAt(e.getPosLine(),MathUtils.realModulo(e.getPosCol()+1, tailleMaxCol));
				break;
			case "S":
			case "D":
				c = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()+1, tailleMaxLine), e.getPosCol());
				break;
			case "NE":
				c = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()-1, tailleMaxLine), MathUtils.realModulo(e.getPosCol()+1, tailleMaxCol));
				break;
			case "NW":
				c = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()-1, tailleMaxLine), MathUtils.realModulo(e.getPosCol()-1, tailleMaxCol));
				break;
			case "SE":
				c = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()+1, tailleMaxLine),MathUtils.realModulo(e.getPosCol()+1, tailleMaxCol));
				break;
			case "SW":
				c = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()+1, tailleMaxLine), MathUtils.realModulo(e.getPosCol()-1, tailleMaxCol));
				break;
				
			default:
				c = e.getMap().getCaseAt(MathUtils.realModulo(e.getPosLine()-1, tailleMaxLine), e.getPosCol());
				break;
			
			}
			
		}
		
		if(c == null) {
			return false;
		}
		
		if((c.getTypeCase() == TypeCase.NORMALGROUND || c.getTypeCase() == TypeCase.WATERGROUND || c.getTypeCase() == TypeCase.FIREGROUND) && !c.hasGameItem()) {
			/*
			 * Créer la nouvelle entité à la nouvelle position
			 */
			
			Entity entity;
			
			switch(e.getTypeEntity()) {
			
			case TypeEntity.HOUND:
				entity = new Hound(c.getLine(), c.getColumn(), e.getMap());
				break;
			case TypeEntity.SLIME:
				entity = new Slime(c.getLine(), c.getColumn(), e.getMap());
				break;
			case TypeEntity.ZOMBIE:
				entity = new Zombie(c.getLine(), c.getColumn(), e.getMap());
				break;
			case TypeEntity.JFUSION:
				return false;
				//break;
			case TypeEntity.PLAYER1:
				entity = new Player(c.getLine(), c.getColumn(), e.getMap());
				break;
			case TypeEntity.OMBRE:
				return false;
				//break;
			default:
				return false;
			
			}
			
			
			c.setGameItem(entity);
			
			e.getAutomata().getController().getGame().addNewEntities(entity);
			
			return true;
		}
		
		return false;
		
		
	}
	
	

}
