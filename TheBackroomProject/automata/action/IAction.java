package action;

import model.entity.Entity;

public interface IAction {
	
	public boolean apply(Entity e);
	
	/**
	 * 
	 * @return int
	 * get de percent dans les IAction
	 */
	public int getpercent();

}
