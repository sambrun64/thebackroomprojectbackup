package action;

import java.util.List;

import model.entity.Entity;

public class ActWizz implements IAction {
	private int percent;
	private List<String> param;

	public ActWizz(List<String> parametre, int percent) {
		this.param = parametre;
		this.percent = percent;
	}

	public int getpercent() {
		return this.percent;
	}
	
	/**
	 * @param Entity e
	 * @return boolean
	 * Apelle wizz dans l'Entity e
	 */
	@Override
	public boolean apply(Entity e) {
		if(this.param.size() == 0 || this.param == null) {
			this.param.add("F");
		}
		
		String str = this.param.get(0);
		switch(str){	
		case "F":
		case "N":
		case "E":
		case "R":
		case "W":
		case "L":
		case "S":
		case "B":
		case "H":
			e.wizz(str);
			return true;
		default:
			return false;
		}
	}
}
